import React, { Component } from "react";

import Can from "../../components/Can";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ConfiguracoesActions from "../../store/ducks/configuracoes";
import EmpresasActions from "../../store/ducks/empresas";
import {
  EmpresaNotFound,
  ConfigProdutoServicoNotFound
} from "../../components/PagesNotFound";

import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Button,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  Label,
  FormGroup,
  FormFeedback,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from "reactstrap";
import { Loading } from "../../components/Loading";
import FabButton from "../../styles/components/FabButton";
import { formatDate } from "../../util/format";
import { percentMask } from "../../util/mask";

class Configuracoes extends Component {
  initial_state = {
    id: "",
    tipo: "",
    percentual: "",
    validade_cashback: ""
  };

  state = {
    ...this.initial_state,
    modalTitle: "",
    activeEmpresa: null
  };

  componentDidMount() {
    const { configuracoes, activeEmpresa } = this.props;

    if (activeEmpresa) {
      this.getData(configuracoes.configuracaoCurrentPage);
    }
  }

  getData() {
    const { configuracoes, getConfiguracoesRequest } = this.props;

    getConfiguracoesRequest(
      configuracoes.configuracaoCurrentPage,
      configuracoes.configuracaoPerPage,
      configuracoes.configuracaoFilter
    );
  }

  refreshPage() {
    this.getData();
  }

  async handleFilter(e) {
    const { setConfiguracaoFilter, setConfiguracaoCurrentPage } = this.props;

    await setConfiguracaoFilter(e.target.value);
    await setConfiguracaoCurrentPage(1);
    this.getData();
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ triedToSubmit: true });
    const { submitConfiguracaoRequest } = this.props;
    const { id, tipo, percentual, validade_cashback } = this.state;

    submitConfiguracaoRequest(id, tipo, percentual, validade_cashback);
  }

  handleCreate() {
    this.clearFields();
    this.setState({ modalTitle: "Cadastro de produtos/serviços" });
    this.toggleModal();
  }

  handleEdit(configuracao) {
    this.setState({
      ...configuracao,
      modalTitle: "Editando produto/serviço"
    });

    this.toggleModal();
  }

  handleDelete(id) {
    this.setState({ id });
    this.toggleModalDelete();
  }

  handleConfirmDelete() {
    const { deleteConfiguracaoRequest } = this.props;
    const { id } = this.state;

    deleteConfiguracaoRequest(id);
  }

  toggleModal() {
    const { toggleConfiguracaoModal } = this.props;
    toggleConfiguracaoModal();
  }

  toggleModalDelete() {
    const { toggleConfiguracaoDeleteModal } = this.props;
    toggleConfiguracaoDeleteModal();
  }

  clearFields() {
    this.setState(this.initial_state);
  }

  cancelSubmit = () => {
    this.clearFields();
    this.toggleModal();
  };

  cancelDelete = () => {
    this.clearFields();
    this.toggleModalDelete();
  };

  handleSelectPage(page) {
    this.getData(page);
    const { setConfiguracaoCurrentPage } = this.props;
    setConfiguracaoCurrentPage(page);
    this.getData();
  }

  render() {
    const {
      tipo,
      percentual,
      validade_cashback,
      modalTitle,
      triedToSubmit
    } = this.state;

    const { configuracoes, activeEmpresa } = this.props;

    if (activeEmpresa === null) return <EmpresaNotFound />;

    // Cria um array para a paginação
    let pagination = [];
    for (let index = 1; index <= configuracoes.result.lastPage; index++) {
      pagination.push(index);
    }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <span className="float-left pt-1">
                  <i className="fa fa-shopping-bag mr-2"></i> Produtos e
                  serviços
                </span>
                <span className="float-right ml-2">
                  <div className="button-group-icons-only">
                    <Button
                      style={{ padding: "0 8px" }}
                      onClick={() => this.refreshPage()}
                    >
                      <i className="fa fa-refresh"></i>
                    </Button>
                  </div>
                </span>
                <span className="float-right pt-1">
                  Total de items: {configuracoes.result.total}
                </span>
              </CardHeader>
              <CardBody>
                {/* Campo de pesquisa */}
                <Input
                  type="search"
                  id="filter"
                  name="filter"
                  value={configuracoes.configuracaoFilter}
                  onChange={e => this.handleFilter(e)}
                  placeholder="Digite para pesquisar..."
                />

                {configuracoes.isLoading && (
                  <div className="my-5">
                    <Loading color="info" textAlign="text-center" size="md" />
                  </div>
                )}

                {!configuracoes.isLoading &&
                  configuracoes.result.data.length === 0 && (
                    <ConfigProdutoServicoNotFound />
                  )}

                {!configuracoes.isLoading &&
                  !(configuracoes.result.data.length === 0) && (
                    <React.Fragment>
                      <Table responsive striped className="mt-3">
                        <thead>
                          <tr>
                            <th>DESCRIÇÃO</th>
                            <th>PERCENTUAL (%)</th>
                            <th>VALIDADE CASHBACK</th>
                            <th>DATA</th>
                            <th width="5%"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {configuracoes.result.data.map(configuracao => (
                            <tr key={configuracao.id}>
                              <td>{configuracao.tipo}</td>
                              <td>{configuracao.percentual} %</td>
                              <td>{configuracao.validade_cashback} meses</td>
                              <td>{formatDate(configuracao.updated_at)}</td>
                              <td style={{ minWidth: "120px" }}>
                                <div className="button-group-icons-only">
                                  <Can checkRole="admin">
                                    <Button
                                      style={{ padding: "0 8px" }}
                                      onClick={() =>
                                        this.handleEdit(configuracao)
                                      }
                                    >
                                      <i className="fa fa-pencil-square-o text-primary"></i>
                                    </Button>
                                  </Can>
                                  <Can checkRole="admin">
                                    <Button
                                      style={{ padding: "0 8px" }}
                                      onClick={() =>
                                        this.handleDelete(configuracao.id)
                                      }
                                    >
                                      <i className="fa fa-trash-o text-danger"></i>
                                    </Button>
                                  </Can>
                                </div>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </Table>

                      {/* Paginação */}
                      <Pagination>
                        <PaginationItem
                          disabled={configuracoes.result.page === 1}
                        >
                          <PaginationLink
                            previous
                            tag="button"
                            onClick={() =>
                              this.handleSelectPage(
                                configuracoes.ConfiguracaoCurrentPage - 1
                              )
                            }
                          >
                            Ant.
                          </PaginationLink>
                        </PaginationItem>
                        {pagination.map((page, key) => (
                          <PaginationItem
                            key="key"
                            active={configuracoes.result.page === page}
                          >
                            <PaginationLink
                              tag="button"
                              onClick={() => this.handleSelectPage(page)}
                            >
                              {page}
                            </PaginationLink>
                          </PaginationItem>
                        ))}
                        <PaginationItem
                          disabled={
                            configuracoes.result.page ===
                            configuracoes.result.lastPage
                          }
                        >
                          <PaginationLink
                            next
                            tag="button"
                            onClick={() =>
                              this.handleSelectPage(
                                configuracoes.ConfiguracaoCurrentPage + 1
                              )
                            }
                          >
                            Próx.
                          </PaginationLink>
                        </PaginationItem>
                      </Pagination>
                    </React.Fragment>
                  )}
              </CardBody>
            </Card>
          </Col>
        </Row>

        {/* Modal */}
        <Modal
          isOpen={configuracoes.configuracaoModalOpen}
          toggle={() => this.toggleModal()}
        >
          <Form onSubmit={e => this.handleSubmit(e)}>
            <ModalHeader toggle={() => this.toggleModal()}>
              {modalTitle}
            </ModalHeader>
            <ModalBody>
              <FormGroup>
                <Label for="tipo">Descrição</Label>
                <Input
                  type="text"
                  id="tipo"
                  name="tipo"
                  value={tipo}
                  onChange={e => this.setState({ tipo: e.target.value })}
                  placeholder="Descrição do produto/serviço"
                  maxLength="255"
                  nvalid={triedToSubmit && tipo === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
              <Row>
                <Col xs="6">
                  <FormGroup>
                    <Label for="percentual">Percentual</Label>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="fa fa-percent"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        type="numeric"
                        id="percentual"
                        name="percentual"
                        value={percentual}
                        onChange={e =>
                          this.setState({
                            percentual: percentMask(e.target.value)
                          })
                        }
                        placeholder="10.00"
                        // maxLength="5"
                        nvalid={triedToSubmit && percentual === ""}
                      />
                    </InputGroup>
                    <FormFeedback>Campo obrigatório</FormFeedback>
                  </FormGroup>
                </Col>

                <Col xs="6">
                  <FormGroup>
                    <Label for="validade_cashback">Validade do cashback</Label>
                    <InputGroup>
                      <Input
                        type="number"
                        id="validade_cashback"
                        name="validade_cashback"
                        value={validade_cashback}
                        onChange={e =>
                          this.setState({ validade_cashback: e.target.value })
                        }
                        placeholder="24"
                        maxLength="2"
                        nvalid={triedToSubmit && validade_cashback === ""}
                      />

                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <span>meses</span>
                        </InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    <FormFeedback>Campo obrigatório</FormFeedback>
                  </FormGroup>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="success" type="submit">
                Salvar
              </Button>{" "}
              <Button color="secondary" onClick={() => this.cancelSubmit()}>
                Cancelar
              </Button>
            </ModalFooter>
          </Form>
        </Modal>

        {/* Modal de Exclusão */}
        <Modal
          isOpen={configuracoes.configuracaoDeleteModalOpen}
          toggle={() => this.toggleModalDelete()}
        >
          <ModalHeader toggle={() => this.toggleModalDelete()}>
            Confirmar
          </ModalHeader>
          <ModalBody>Deseja realmente excluir esse item?</ModalBody>
          <ModalFooter>
            <Button
              color="secondary"
              onClick={() => this.handleConfirmDelete()}
            >
              Sim
            </Button>
            <Button color="danger" onClick={() => this.cancelDelete()}>
              Não
            </Button>
          </ModalFooter>
        </Modal>
        <Can checkRole="admin">
          <FabButton onClick={() => this.handleCreate()}>
            <i className="fa fa-plus"></i>
          </FabButton>
        </Can>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  activeEmpresa: state.empresas.active,
  configuracoes: state.configuracoes
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...EmpresasActions, ...ConfiguracoesActions }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Configuracoes);
