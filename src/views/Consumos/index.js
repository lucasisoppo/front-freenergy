import React, { Component } from "react";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ConsumosActions from "../../store/ducks/consumos";
import EmpresasActions from "../../store/ducks/empresas";

import Api from "../../services/api";

import Select from "react-select";
import {
  formatDate,
  formatHourMinutes,
  formatWeek,
  formatPrice,
} from "../../util/format";
import {
  EmpresaNotFound,
  ConsumosNotFound,
} from "../../components/PagesNotFound";

import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Button,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  Label,
  FormGroup,
  FormFeedback,
} from "reactstrap";
import { Loading } from "../../components/Loading";
//  import FabButton from "../../styles/components/FabButton";

class Consumos extends Component {
  initial_state = {
    id: "",
    media_corrente: "",
    mediaPotencia: "",
    data_hora: "",
    duracao: "",
    selected_equipamento: "",
    triedToSubmit: false,
  };

  state = {
    ...this.initial_state,
    modalTitle: "",
    equipamentos: [],
    activeEmpresa: null,
  };

  async componentDidMount() {
    const { consumos, activeEmpresa } = this.props;

    if (activeEmpresa) {
      this.getData(consumos.consumoCurrentPage);
      await this.getDataEquipamentos();
    }
  }

  getData() {
    const { consumos, getConsumosRequest } = this.props;

    getConsumosRequest(
      consumos.consumoCurrentPage,
      consumos.consumoPerPage,
      consumos.consumoFilter
    );
  }

  async getDataEquipamentos() {
    const res = await Api.get("/equipamentos/1/9999");

    this.setState({ equipamentos: res.data.data });
  }

  refreshPage() {
    this.getData();
  }

  async handleFilter(e) {
    const { setConsumoFilter, setConsumoCurrentPage } = this.props;

    await setConsumoFilter(e.target.value);
    await setConsumoCurrentPage(1);
    this.getData();
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ triedToSubmit: true });
    const { submitConsumoRequest } = this.props;
    const {
      id,
      selected_equipamento,
      media_corrente,
      duracao,
      data_hora
    } = this.state;

    if (!selected_equipamento) {
      return false;
    }

    submitConsumoRequest(
      id,
      +selected_equipamento.id,
      media_corrente,
      duracao,
      data_hora

    );
  }

  handleCreate() {
    this.clearFields();
    this.setState({ modalTitle: "Cadastro de consumos" });
    this.toggleModal();
  }

  handleEdit(consumo) {
    this.setState({
      ...consumo,
      selected_equipamento: consumo.equipamento,
      modalTitle: "Editando consumo",
    });

    this.toggleModal();
  }

  handleDelete(id) {
    this.setState({ id });
    this.toggleModalDelete();
  }

  handleConfirmDelete() {
    const { deleteConsumoRequest } = this.props;
    const {
      id,
      equipamento_id,
      media_corrente,
      duracao,
      data_hora
    } = this.state;

    deleteConsumoRequest(
      id,
      equipamento_id,
      media_corrente,
      duracao,
      data_hora
    );
  }

  toggleModal() {
    const { toggleConsumoModal } = this.props;
    toggleConsumoModal();
  }

  toggleModalDelete() {
    const { toggleConsumoDeleteModal } = this.props;
    toggleConsumoDeleteModal();
  }

  clearFields() {
    this.setState(this.initial_state);
  }

  cancelSubmit = () => {
    this.clearFields();
    this.toggleModal();
  };

  cancelDelete = () => {
    this.clearFields();
    this.toggleModalDelete();
  };

  async handleSelectPage(page) {
    const { setConsumoCurrentPage } = this.props;
    await setConsumoCurrentPage(page);
    this.getData();
  }

  render() {
    const {
      media_corrente,
      data_hora,
      duracao,
      equipamentos,
      selected_equipamento,
      modalTitle,
      triedToSubmit,
    } = this.state;

    const { consumos, activeEmpresa } = this.props;

    if (activeEmpresa === null) return <EmpresaNotFound />;

    // Cria um array para a paginação
    let pagination = [];
    for (let index = 1; index <= consumos.result.lastPage; index++) {
      pagination.push(index);
    }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <span className="float-left pt-1">
                  <i className="fa fa-cog mr-2"></i> Consumos
                </span>
                <span className="float-right ml-2">
                  <div className="button-group-icons-only">
                    <Button
                      style={{ padding: "0 8px" }}
                      onClick={() => this.refreshPage()}
                    >
                      <i className="fa fa-refresh"></i>
                    </Button>
                  </div>
                </span>
                <span className="float-right pt-1">
                  Total de items: {consumos.result.total}
                </span>
              </CardHeader>
              <CardBody>
                {/* Campo de pesquisa */}
                <Input
                  type="search"
                  id="filter"
                  name="filter"
                  value={consumos.consumoFilter}
                  onChange={(e) => this.handleFilter(e)}
                  placeholder="Digite para pesquisar..."
                />

                {consumos.isLoading && (
                  <div className="my-5">
                    <Loading color="info" textAlign="text-center" size="md" />
                  </div>
                )}

                {!consumos.isLoading && consumos.result.data.length === 0 && (
                  <ConsumosNotFound />
                )}

                {!consumos.isLoading && !(consumos.result.data.length === 0) && (
                  <React.Fragment>
                    {/* Tabela */}
                    <Table responsive striped className="mt-3">
                      <thead>
                        <tr>
                          <th>EQUIPAMENTO</th>
                          <th>MÉDIA CORRENTE</th>
                          <th>MÉDIA POTENCIA</th>
                          <th>INÍCIO DO CONSUMO</th>
                          <th>DURAÇÃO</th>
                          <th style={{ textAlign: "left" }}>CUSTO TOTAL</th>
                          <th width="5%"></th>
                        </tr>
                      </thead>
                      <tbody>
                        {consumos.result.data.map((consumo) => (
                          <tr key={consumo.id}>
                            <td>{consumo.equipamento.descricao}</td>
                            <td>{consumo.media_corrente} ampere</td>
                            <td>{consumo.media_potencia} watts</td>

                            <td style={{ width: "250px" }}>
                              {formatWeek(consumo.data_hora).substr(0, 3) +
                                ", " +
                                formatDate(consumo.data_hora) +
                                ", " +
                                formatHourMinutes(consumo.data_hora)}
                            </td>

                            <td>{consumo.duracao / 60} minutos</td>
                            <td
                              className="font-weight-bold"
                              style={{ color: "#f63c3a", textAlign: "left" }}
                            >
                              {formatPrice(consumo.custo_total)}
                            </td>
                            <td style={{ minWidth: "120px" }}>
                              <div className="button-group-icons-only">
                                <Button
                                  style={{ padding: "0 8px" }}
                                  onClick={() => this.handleEdit(consumo)}
                                >
                                  <i className="fa fa-pencil-square-o text-primary"></i>
                                </Button>
                                <Button
                                  style={{ padding: "0 8px" }}
                                  onClick={() => this.handleDelete(consumo.id)}
                                >
                                  <i className="fa fa-trash-o text-danger"></i>
                                </Button>
                              </div>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>

                    {/* Paginação */}
                    <Pagination>
                      <PaginationItem disabled={consumos.result.page === 1}>
                        <PaginationLink
                          previous
                          tag="button"
                          onClick={() =>
                            this.handleSelectPage(
                              consumos.consumoCurrentPage - 1
                            )
                          }
                        >
                          Ant.
                        </PaginationLink>
                      </PaginationItem>
                      {pagination.map((page, key) => (
                        <PaginationItem
                          key="key"
                          active={consumos.result.page === page}
                        >
                          <PaginationLink
                            tag="button"
                            onClick={() => this.handleSelectPage(page)}
                          >
                            {page}
                          </PaginationLink>
                        </PaginationItem>
                      ))}
                      <PaginationItem
                        disabled={
                          consumos.result.page === consumos.result.lastPage
                        }
                      >
                        <PaginationLink
                          next
                          tag="button"
                          onClick={() =>
                            this.handleSelectPage(
                              consumos.consumoCurrentPage + 1
                            )
                          }
                        >
                          Próx.
                        </PaginationLink>
                      </PaginationItem>
                    </Pagination>
                  </React.Fragment>
                )}
              </CardBody>
            </Card>
          </Col>
        </Row>

        {/* Modal */}
        <Modal
          isOpen={consumos.consumoModalOpen}
          toggle={() => this.toggleModal()}
        >
          <Form onSubmit={(e) => this.handleSubmit(e)}>
            <ModalHeader toggle={() => this.toggleModal()}>
              {modalTitle}
            </ModalHeader>
            <ModalBody>
              <FormGroup>
                <Label>EQUIPAMENTO</Label>
                <Select
                  isClearable={true}
                  isSearchable={true}
                  options={equipamentos}
                  value={selected_equipamento}
                  getOptionLabel={(equipamento) => equipamento.descricao}
                  getOptionValue={(equipamento) => equipamento.id}
                  onChange={(equipamento) =>
                    this.setState({ selected_equipamento: equipamento })
                  }
                  placeholder="Selecione o equipamento"
                />
              </FormGroup>
              <FormGroup>
                <Label for="media_corrente">MÉDIA CORRENTE</Label>
                <Input
                  type="text"
                  id="media_corrente"
                  name="media_corrente"
                  value={media_corrente}
                  onChange={(e) =>
                    this.setState({ media_corrente: e.target.value })
                  }
                  placeholder="Descrição do equipamento"
                  invalid={triedToSubmit && media_corrente === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label for="duracao">DURAÇÃO</Label>
                <Input
                  type="text"
                  id="duracao"
                  name="duracao"
                  value={duracao}
                  onChange={(e) => this.setState({ duracao: e.target.value })}
                  placeholder="Descrição do equipamento"
                  invalid={triedToSubmit && duracao === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label for="data_hora">DATA HORA</Label>
                <Input
                  type="text"
                  id="data_hora"
                  name="data_hora"
                  value={data_hora}
                  onChange={(e) => this.setState({ data_hora: e.target.value })}
                  placeholder="Descrição do equipamento"
                  invalid={triedToSubmit && data_hora === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
            </ModalBody>
            <ModalFooter>
              <Button color="success" type="submit">
                Salvar
              </Button>{" "}
              <Button color="secondary" onClick={() => this.cancelSubmit()}>
                Cancelar
              </Button>
            </ModalFooter>
          </Form>
        </Modal>

        {/* Modal de Exclusão */}
        <Modal
          isOpen={consumos.consumoDeleteModalOpen}
          toggle={() => this.toggleModalDelete()}
        >
          <ModalHeader toggle={() => this.toggleModalDelete()}>
            Confirmar
          </ModalHeader>
          <ModalBody>Deseja realmente excluir esse item?</ModalBody>
          <ModalFooter>
            <Button
              color="secondary"
              onClick={() => this.handleConfirmDelete()}
            >
              Sim
            </Button>{" "}
            <Button color="danger" onClick={() => this.cancelDelete()}>
              Não
            </Button>
          </ModalFooter>
        </Modal>

        {/* <FabButton onClick={() => this.handleCreate()}>
          <i className="fa fa-plus"></i>
        </FabButton> */}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  activeEmpresa: state.empresas.active,
  consumos: state.consumos,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...EmpresasActions, ...ConsumosActions }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Consumos);
