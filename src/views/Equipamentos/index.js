import React, { Component } from "react";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import EquipamentosActions from "../../store/ducks/equipamentos";
import EmpresasActions from "../../store/ducks/empresas";
import Api from "../../services/api";
import {
  EmpresaNotFound,
  EquipamentosNotFound,
} from "../../components/PagesNotFound";

import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Button,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  Label,
  FormGroup,
  FormFeedback,
} from "reactstrap";
import Select from "react-select";
import { Loading } from "../../components/Loading";
import FabButton from "../../styles/components/FabButton";

class Equipamentos extends Component {
  initial_state = {
    id: "",
    descricao: "",
    potencia: "",
    selected_grupo: "",
    selected_setor: "",
    triedToSubmit: false,
  };

  state = {
    ...this.initial_state,
    modalTitle: "",
    grupos: [],
    setors: [],
    activeEmpresa: null,
  };

  async componentDidMount() {
    const { equipamentos, activeEmpresa } = this.props;

    if (activeEmpresa) {
      this.getData(equipamentos.equipamentoCurrentPage);
      await this.getDataGrupos();
      await this.getDataSetors();
    }
  }

  getData() {
    const { equipamentos, getEquipamentosRequest } = this.props;

    getEquipamentosRequest(
      equipamentos.equipamentoCurrentPage,
      equipamentos.equipamentoPerPage,
      equipamentos.equipamentoFilter
    );
  }

  async getDataGrupos() {
    const res = await Api.get("/grupos/1/9999");

    this.setState({ grupos: res.data.data });
  }

  async getDataSetors() {
    const res = await Api.get("/setors/1/9999");

    this.setState({ setors: res.data.data });
  }

  refreshPage() {
    this.getData();
  }

  async handleFilter(e) {
    const { setEquipamentoFilter, setEquipamentoCurrentPage } = this.props;

    await setEquipamentoFilter(e.target.value);
    await setEquipamentoCurrentPage(1);
    this.getData();
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ triedToSubmit: true });
    const { submitEquipamentoRequest } = this.props;
    const { id, descricao, potencia, selected_grupo, selected_setor } = this.state;

    if (!selected_grupo) {
      return false;
    }

    if (!selected_setor) {
      return false;
    }

    submitEquipamentoRequest(
      id,
      descricao,
      potencia,
      +selected_grupo.id,
      +selected_setor.id
    );
  }

  handleCreate() {
    this.clearFields();
    this.setState({ modalTitle: "Cadastro de equipamentos" });
    this.toggleModal();
  }

  handleEdit(equipamento) {
    this.setState({
      ...equipamento,
      selected_grupo: equipamento.grupo,
      selected_setor: equipamento.setor,
      modalTitle: "Editando equipamento",
    });

    this.toggleModal();
  }

  handleDelete(id) {
    this.setState({ id });
    this.toggleModalDelete();
  }

  handleConfirmDelete() {
    const { deleteEquipamentoRequest } = this.props;
    const { id, descricao, potencia, grupo_id, setor_id } = this.state;

    deleteEquipamentoRequest(id, descricao, potencia, grupo_id, setor_id);
  }

  toggleModal() {
    const { toggleEquipamentoModal } = this.props;
    toggleEquipamentoModal();
  }

  toggleModalDelete() {
    const { toggleEquipamentoDeleteModal } = this.props;
    toggleEquipamentoDeleteModal();
  }

  clearFields() {
    this.setState(this.initial_state);
  }

  cancelSubmit = () => {
    this.clearFields();
    this.toggleModal();
  };

  cancelDelete = () => {
    this.clearFields();
    this.toggleModalDelete();
  };

  async handleSelectPage(page) {
    const { setEquipamentoCurrentPage } = this.props;
    await setEquipamentoCurrentPage(page);
    this.getData();
  }

  render() {
    const {
      descricao,
      potencia,
      grupos,
      setors,
      selected_grupo,
      selected_setor,
      modalTitle,
      triedToSubmit,
    } = this.state;

    const { equipamentos, activeEmpresa } = this.props;

    if (activeEmpresa === null) return <EmpresaNotFound />;

    // Cria um array para a paginação
    let pagination = [];
    for (let index = 1; index <= equipamentos.result.lastPage; index++) {
      pagination.push(index);
    }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <span className="float-left pt-1">
                  <i className="fa fa-cog mr-2"></i> Equipamentos
                </span>
                <span className="float-right ml-2">
                  <div className="button-group-icons-only">
                    <Button
                      style={{ padding: "0 8px" }}
                      onClick={() => this.refreshPage()}
                    >
                      <i className="fa fa-refresh"></i>
                    </Button>
                  </div>
                </span>
                <span className="float-right pt-1">
                  Total de items: {equipamentos.result.total}
                </span>
              </CardHeader>
              <CardBody>
                {/* Campo de pesquisa */}
                <Input
                  type="search"
                  id="filter"
                  name="filter"
                  value={equipamentos.equipamentoFilter}
                  onChange={(e) => this.handleFilter(e)}
                  placeholder="Digite para pesquisar..."
                />

                {equipamentos.isLoading && (
                  <div className="my-5">
                    <Loading color="info" textAlign="text-center" size="md" />
                  </div>
                )}

                {!equipamentos.isLoading &&
                  equipamentos.result.data.length === 0 && (
                    <EquipamentosNotFound />
                  )}

                {!equipamentos.isLoading &&
                  !(equipamentos.result.data.length === 0) && (
                    <React.Fragment>
                      {/* Tabela */}
                      <Table responsive striped className="mt-3">
                        <thead>
                          <tr>
                            <th>DESCRIÇÃO</th>
                            <th>POTENCIA</th>
                            <th>GRUPO</th>
                            <th>SETOR</th>
                            <th width="5%"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {equipamentos.result.data.map((equipamento) => (
                            <tr key={equipamento.id}>
                              <td>{equipamento.descricao}</td>
                              <td>{equipamento.potencia} watts</td>
                              <td>{equipamento.grupo.descricao}</td>
                              <td>{equipamento.setor.descricao}</td>
                              <td style={{ minWidth: "120px" }}>
                                <div className="button-group-icons-only">
                                  <Button
                                    style={{ padding: "0 8px" }}
                                    onClick={() => this.handleEdit(equipamento)}
                                  >
                                    <i className="fa fa-pencil-square-o text-primary"></i>
                                  </Button>
                                  <Button
                                    style={{ padding: "0 8px" }}
                                    onClick={() =>
                                      this.handleDelete(equipamento.id)
                                    }
                                  >
                                    <i className="fa fa-trash-o text-danger"></i>
                                  </Button>
                                </div>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </Table>

                      {/* Paginação */}
                      <Pagination>
                        <PaginationItem
                          disabled={equipamentos.result.page === 1}
                        >
                          <PaginationLink
                            previous
                            tag="button"
                            onClick={() =>
                              this.handleSelectPage(
                                equipamentos.equipamentoCurrentPage - 1
                              )
                            }
                          >
                            Ant.
                          </PaginationLink>
                        </PaginationItem>
                        {pagination.map((page, key) => (
                          <PaginationItem
                            key="key"
                            active={equipamentos.result.page === page}
                          >
                            <PaginationLink
                              tag="button"
                              onClick={() => this.handleSelectPage(page)}
                            >
                              {page}
                            </PaginationLink>
                          </PaginationItem>
                        ))}
                        <PaginationItem
                          disabled={
                            equipamentos.result.page ===
                            equipamentos.result.lastPage
                          }
                        >
                          <PaginationLink
                            next
                            tag="button"
                            onClick={() =>
                              this.handleSelectPage(
                                equipamentos.equipamentoCurrentPage + 1
                              )
                            }
                          >
                            Próx.
                          </PaginationLink>
                        </PaginationItem>
                      </Pagination>
                    </React.Fragment>
                  )}
              </CardBody>
            </Card>
          </Col>
        </Row>

        {/* Modal */}
        <Modal
          isOpen={equipamentos.equipamentoModalOpen}
          toggle={() => this.toggleModal()}
        >
          <Form onSubmit={(e) => this.handleSubmit(e)}>
            <ModalHeader toggle={() => this.toggleModal()}>
              {modalTitle}
            </ModalHeader>
            <ModalBody>
              <FormGroup>
                <Label for="descricao">DESCRIÇÃO</Label>
                <Input
                  type="text"
                  id="descricao"
                  name="descricao"
                  value={descricao}
                  onChange={(e) => this.setState({ descricao: e.target.value })}
                  placeholder="Descrição do equipamento"
                  invalid={triedToSubmit && descricao === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label for="descricao">POTENCIA</Label>
                <Input
                  type="text"
                  id="potencia"
                  name="potencia"
                  value={potencia}
                  onChange={(e) => this.setState({ potencia: e.target.value })}
                  placeholder="Descrição do equipamento"
                  invalid={triedToSubmit && potencia === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label>GRUPO</Label>
                <Select
                  isClearable={true}
                  isSearchable={true}
                  options={grupos}
                  value={selected_grupo}
                  getOptionLabel={(grupo) => grupo.descricao}
                  getOptionValue={(grupo) => grupo.id}
                  onChange={(grupo) => this.setState({ selected_grupo: grupo })}
                  placeholder="Selecione o grupo"
                />
              </FormGroup>
              <FormGroup>
                <Label>SETOR</Label>
                <Select
                  isClearable={true}
                  isSearchable={true}
                  options={setors}
                  value={selected_setor}
                  getOptionLabel={(setor) => setor.descricao}
                  getOptionValue={(setor) => setor.id}
                  onChange={(setor) => this.setState({ selected_setor: setor })}
                  placeholder="Selecione o Setor"
                />
              </FormGroup>
            </ModalBody>
            <ModalFooter>
              <Button color="success" type="submit">
                Salvar
              </Button>{" "}
              <Button color="secondary" onClick={() => this.cancelSubmit()}>
                Cancelar
              </Button>
            </ModalFooter>
          </Form>
        </Modal>

        {/* Modal de Exclusão */}
        <Modal
          isOpen={equipamentos.equipamentoDeleteModalOpen}
          toggle={() => this.toggleModalDelete()}
        >
          <ModalHeader toggle={() => this.toggleModalDelete()}>
            Confirmar
          </ModalHeader>
          <ModalBody>Deseja realmente excluir esse item?</ModalBody>
          <ModalFooter>
            <Button
              color="secondary"
              onClick={() => this.handleConfirmDelete()}
            >
              Sim
            </Button>{" "}
            <Button color="danger" onClick={() => this.cancelDelete()}>
              Não
            </Button>
          </ModalFooter>
        </Modal>

        <FabButton onClick={() => this.handleCreate()}>
          <i className="fa fa-plus"></i>
        </FabButton>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  activeEmpresa: state.empresas.active,
  equipamentos: state.equipamentos,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...EmpresasActions, ...EquipamentosActions }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Equipamentos);
