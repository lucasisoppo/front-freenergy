import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  FormFeedback
} from "reactstrap";
import logo from "../../../assets/logo.svg";
import { cpfMask } from "../../../util/mask";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import AuthActions from "../../../store/ducks/auth";

class Registro extends Component {
  state = {
    nome: "",
    cpf: "",
    email: "",
    password: "",
    confirmPassword: "",
    triedToSubmit: false
  };

  handleSubmit = e => {
    e.preventDefault();

    this.setState({ triedToSubmit: true });
    const { nome, cpf, email, password, confirmPassword } = this.state;
    const { registroRequest } = this.props;

    registroRequest(nome, cpf, email, password, confirmPassword);
  };

  handleInputChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const { nome, cpf, email, password, confirmPassword, triedToSubmit } = this.state;

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8" lg="6" xl="5">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={this.handleSubmit}>
                      <img
                        style={{
                          flex: 1,
                          maxWidth: "100%"
                        }}
                        src={logo}
                        alt="Freeenergy"
                      />
                      <h2 className="text-center py-4">Criar conta</h2>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          name="nome"
                          placeholder="Nome completo"
                          value={nome}
                          onChange={this.handleInputChange}
                          invalid={triedToSubmit && nome === ""}
                        />
                        <FormFeedback>Campo obrigatório</FormFeedback>
                      </InputGroup>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="cui-credit-card "></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="text"
                          id="cpf"
                          name="cpf"
                          value={cpf}
                          onChange={e =>
                            this.setState({ cpf: cpfMask(e.target.value) })
                          }
                          placeholder="CPF"
                          maxLength="14"
                          invalid={triedToSubmit && cpf === ""}
                        />
                        <FormFeedback>Campo obrigatório</FormFeedback>
                      </InputGroup>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="fa fa-envelope-o"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="email"
                          name="email"
                          placeholder="exemplo@exemplo.com"
                          value={email}
                          onChange={this.handleInputChange}
                          invalid={triedToSubmit && email === ""}
                        />
                        <FormFeedback>Campo obrigatório</FormFeedback>
                      </InputGroup>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          name="password"
                          value={password}
                          onChange={this.handleInputChange}
                          placeholder="Senha"
                          invalid={triedToSubmit && password === ""}
                        />
                        <FormFeedback>Campo obrigatório</FormFeedback>
                      </InputGroup>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          name="confirmPassword"
                          value={confirmPassword}
                          onChange={this.handleInputChange}
                          placeholder="Confirmação de Senha"
                          invalid={triedToSubmit && confirmPassword === ""}
                        />
                        <FormFeedback>Campo obrigatório</FormFeedback>
                      </InputGroup>
                      <Button type="submit" color="success" block>
                        CRIAR CONTA
                      </Button>
                    </Form>
                    <Button style={{ marginTop: "20px" }} color="link" block>
                      <Link to="/">Já possui conta?</Link>
                    </Button>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(AuthActions, dispatch);

export default connect(null, mapDispatchToProps)(Registro);
