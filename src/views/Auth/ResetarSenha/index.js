import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  FormFeedback
} from "reactstrap";
import logo from "../../../assets/logo.svg";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import AuthActions from "../../../store/ducks/auth";

class ResetarSenha extends Component {
  state = {
    token: "",
    password: "",
    confirmPassword: "",
    triedToSubmit: false
  };

  handleSubmit = e => {
    e.preventDefault();

    this.setState({ triedToSubmit: true });
    const { token, password, confirmPassword } = this.state;
    const { resetarSenhaRequest } = this.props;

    resetarSenhaRequest(token, password, confirmPassword);
  };

  handleInputChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const { token, password, confirmPassword, triedToSubmit } = this.state;

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8" lg="6" xl="5">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={this.handleSubmit}>
                      <img
                        style={{
                          flex: 1,
                          maxWidth: "100%"
                        }}
                        src={logo}
                        alt="Freeenergy"
                      />
                      <h2 className="text-center py-4">Resetar senha</h2>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="fa fa-unlock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          name="token"
                          value={token}
                          onChange={this.handleInputChange}
                          placeholder="Token de recuperação enviado por e-mail"
                          invalid={triedToSubmit && token === ""}
                        />
                        <FormFeedback>Campo obrigatório</FormFeedback>
                      </InputGroup>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="fa fa-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          name="password"
                          value={password}
                          onChange={this.handleInputChange}
                          placeholder="Nova senha"
                          invalid={triedToSubmit && password === ""}
                        />
                        <FormFeedback>Campo obrigatório</FormFeedback>
                      </InputGroup>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="fa fa-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          name="confirmPassword"
                          value={confirmPassword}
                          onChange={this.handleInputChange}
                          placeholder="Confirmar senha"
                          invalid={triedToSubmit && confirmPassword === ""}
                        />
                        <FormFeedback>Campo obrigatório</FormFeedback>
                      </InputGroup>
                      <Button type="submit" color="success" block>
                        TROCAR SENHA
                      </Button>
                    </Form>
                    <Button style={{ marginTop: "20px" }} color="link" block>
                      <Link to="/login">Fazer login?</Link>
                    </Button>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(AuthActions, dispatch);

export default connect(null, mapDispatchToProps)(ResetarSenha);
