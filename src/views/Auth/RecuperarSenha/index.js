import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  FormFeedback
} from "reactstrap";
import logo from "../../../assets/logo.svg";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import AuthActions from "../../../store/ducks/auth";

class RecuperarSenha extends Component {
  state = {
    email: "",
    triedToSubmit: false
  };

  handleSubmit = e => {
    e.preventDefault();

    this.setState({ triedToSubmit: true });
    const { email } = this.state;
    const { recuperarSenhaRequest } = this.props;

    recuperarSenhaRequest(email);
  };

  handleInputChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const { email, triedToSubmit } = this.state;

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8" lg="6" xl="5">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={this.handleSubmit}>
                      <img
                        style={{
                          flex: 1,
                          maxWidth: "100%"
                        }}
                        src={logo}
                        alt="Freeenergy"
                      />
                      <h2 className="text-center py-4">Recuperar senha</h2>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="fa fa-envelope-o"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="email"
                          name="email"
                          placeholder="Digite seu e-mail"
                          value={email}
                          onChange={this.handleInputChange}
                          invalid={triedToSubmit && email === ""}
                        />
                        <FormFeedback>Campo obrigatório</FormFeedback>
                      </InputGroup>
                      <Button type="submit" color="success" block>
                        RECUPERAR SENHA
                      </Button>
                    </Form>
                    <Button style={{ marginTop: "20px" }} color="link" block>
                      <Link to="/login">Fazer login?</Link>
                    </Button>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(AuthActions, dispatch);

export default connect(null, mapDispatchToProps)(RecuperarSenha);
