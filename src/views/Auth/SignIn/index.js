import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  FormFeedback
} from "reactstrap";

import logo from "../../../assets/logo.svg";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import AuthActions from "../../../store/ducks/auth";

class SignIn extends Component {
  state = {
    email: "",
    password: "",
    triedToSubmit: false
  };

  handleSubmit = e => {
    e.preventDefault();

    this.setState({ triedToSubmit: true });
    const { email, password } = this.state;
    const { signInRequest } = this.props;

    signInRequest(email, password);
  };

  handleInputChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const { email, password, triedToSubmit } = this.state;

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8" lg="6" xl="5">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={this.handleSubmit}>
                      <img
                        style={{
                          margin: "0 0 25px",
                          flex: 1,
                          maxWidth: "100%"
                        }}
                        src={logo}
                        alt="Freeenergy"
                      />
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="fa fa-envelope-o"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="email"
                          name="email"
                          placeholder="exemplo@exemplo.com"
                          value={email}
                          onChange={this.handleInputChange}
                          invalid={triedToSubmit && email === ""}
                        />
                        <FormFeedback>Campo obrigatório</FormFeedback>
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          name="password"
                          value={password}
                          onChange={this.handleInputChange}
                          placeholder="*******"
                          invalid={triedToSubmit && password === ""}
                        />
                        <FormFeedback>Campo obrigatório</FormFeedback>
                      </InputGroup>
                      <Row>
                        <Col>
                          <Button type="submit" color="success" block>
                            ENTRAR
                          </Button>
                        </Col>
                      </Row>
                      <br />
                      <Row>
                        <Col>
                          <Link to="/recuperar-senha">
                            <Button color="link" block>
                              Esqueceu sua senha?
                            </Button>
                          </Link>
                        </Col>
                        <Col>
                          <Link to="/registro">
                            <Button color="link" block>
                              Registre-se agora!
                            </Button>
                          </Link>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(AuthActions, dispatch);

export default connect(null, mapDispatchToProps)(SignIn);
