import React, { Component } from "react";

import { Bar } from "react-chartjs-2";
import "chartjs-plugin-labels";

import { CardBody } from "reactstrap";
import { getStyle } from "@coreui/coreui/dist/js/coreui-utilities";

// const brandSuccess = getStyle("--success");
const brandDark = getStyle("--dark");

const chartOptions = {
  maintainAspectRatio: false,
  scales: {
    yAxes: [
      {
          display: false
      }
    ],
  },
  legend: {
    display: true
  },
  plugins: {
    labels: {
      fontSize: 10,
      fontColor: brandDark,
      precision: 1,
      render: function (args) {
        return 'R$ ' + args.value;
      }
    }
  },
};

class consumoMensal extends Component {
  render() {
    const { consumoMensal } = this.props;
    const anoAnt = consumoMensal && consumoMensal[0] && consumoMensal[0].anoant;
    const anoAtual = consumoMensal && consumoMensal[0] && consumoMensal[0].anoatual;

    const chartData = {
      labels: consumoMensal.map((item) => item.mes),
      datasets: [
        {
          data: consumoMensal.map((item) => {return item.totalanoant}),
          backgroundColor: "#FF7",
          borderWidth: 1,
          label: anoAnt,
        },
        {
          data: consumoMensal.map((item) => item.totalanoatual),
          backgroundColor: "#FF6384",
          borderWidth: 1,
          label: anoAtual,
        }
      ]
    };

    return (
      <div className="animated fadeIn">
        <CardBody>
          <div className="chart-wrapper" style={{ height: "350px" }}>
            <Bar
              data={chartData}
              options={chartOptions}
              height={350}
            />
          </div>
        </CardBody>
      </div>
    );
  }
}

export default consumoMensal;
