// import React, { Component, lazy, Suspense } from 'react';
import React, { Component } from "react";

import api from "../../services/api";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import EmpresasActions from "../../store/ducks/empresas";

import { Row, Col } from "reactstrap";
import { Loading } from "../../components/Loading";
import { EmpresaNotFound } from "../../components/PagesNotFound";
import ListNotFound from "../../components/ListNotFound";
import WrapperCard from "../../components/WrapperCard";

import ConsumoPorGrupos from "./consumoPorGrupos";
import ConsumoGruposPorSetor from "./consumoGruposPorSetor";
import ConsumoMensal from "./consumoMensal";

class Dashboard extends Component {
  state = {
    data: {
      consumoPorGrupos: [],
      consumoGruposPorSetor: [],
      consumoMensal: []
    },
    isLoading: false,
  };

  async componentDidMount() {
    const { activeEmpresa } = this.props;
    if (activeEmpresa) {
      await this.getData();
    }
  }

  async getData() {
    this.toggleLoading(true);

    const response = await api.get("dashboard");
    this.setState({ data: response.data });

    this.toggleLoading(false);
  }

  toggleLoading(is) {
    this.setState({ isLoading: is });
  }

  render() {
    const { activeEmpresa } = this.props;

    if (!activeEmpresa) return <EmpresaNotFound />;

    const { data, isLoading } = this.state;

    return (
      <div className="animated fadeIn">
        <Row>
          <Col md="12">
            <WrapperCard
              title="CONSUMO POR GRUPOS"
              // subtitle="Dez itens com mais vendas no último ano"
            >
              {!isLoading && data.consumoPorGrupos.length > 0 && (
                <ConsumoPorGrupos consumoPorGrupos={data.consumoPorGrupos} />
              )}

              {!isLoading && data.consumoPorGrupos.length === 0 && (
                <ListNotFound
                  text="Não existem consumo ainda..."
                  icon="fa fa-bar-chart"
                  fontSize="130px"
                />
              )}

              {isLoading && (
                <div className="mb-5">
                  <Loading />
                </div>
              )}
            </WrapperCard>
          </Col>
        </Row>
        <Row>
          <Col md="12">
            <WrapperCard
              title="CONSUMO GRUPOS POR SETOR"
              // subtitle="Dez itens com mais vendas no último ano"
            >
              {!isLoading && data.consumoGruposPorSetor.length > 0 && (
                <ConsumoGruposPorSetor consumoGruposPorSetor={data.consumoGruposPorSetor} />
              )}

              {!isLoading && data.consumoGruposPorSetor.length === 0 && (
                <ListNotFound
                  text="Não existem consumo ainda..."
                  icon="fa fa-bar-chart"
                  fontSize="130px"
                />
              )}

              {isLoading && (
                <div className="mb-5">
                  <Loading />
                </div>
              )}
            </WrapperCard>
          </Col>
        </Row>
        <Row>
          <Col md="12">
            <WrapperCard
              title="CONSUMO MENSAL">
               <ConsumoMensal consumoMensal={data.consumoMensal} />
              {/* {!isLoading && data.consumoMensal.length > 0 && (
                <ConsumoMensal consumoMensal={data.consumoMensal} />
              )}

              {!isLoading && data.consumoMensal.length === 0 && (
                <ListNotFound
                  text="Não existem consumo ainda..."
                  icon="fa fa-bar-chart"
                  fontSize="130px"
                />
              )} */}

              {isLoading && (
                <div className="mb-5">
                  <Loading />
                </div>
              )}
            </WrapperCard>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  activeEmpresa: state.empresas.active,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...EmpresasActions }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
