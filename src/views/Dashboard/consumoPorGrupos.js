import React, { Component } from "react";

import { HorizontalBar } from "react-chartjs-2";
import "chartjs-plugin-labels";

import { CardBody } from "reactstrap";
import { getStyle, hexToRgba } from "@coreui/coreui/dist/js/coreui-utilities";

const brandSuccess = getStyle("--success");
const brandDark = getStyle("--dark");

const chartOptions = {
  maintainAspectRatio: false,
  scales: {
    xAxes: [
      {
        ticks: {
          beginAtZero: true
        }
      }
    ],
  },
  legend: {
    display: true,

    onClick: function(e, legendItem) {
      var index = legendItem.datasetIndex;
      var ci = this.chart;
      var alreadyHidden = (ci.getDatasetMeta(index).hidden === null) ? false : ci.getDatasetMeta(index).hidden;

      ci.data.datasets.forEach(function(e, i) {
        var meta = ci.getDatasetMeta(i);

        if (i !== index) {
          if (!alreadyHidden) {
            meta.hidden = meta.hidden === null ? !meta.hidden : null;
          } else if (meta.hidden === null) {
            meta.hidden = true;
          }
        } else if (i === index) {
          meta.hidden = null;
        }
      });

      ci.update();
    },

  },
  plugins: {
    labels: {
      render: "percentage",
      fontColor: brandDark,
      precision: 1,
    },
  },
};

class consumoPorGrupos extends Component {
  render() {
    const { consumoPorGrupos } = this.props;

    const chartData = {
      labels: consumoPorGrupos.map((item) => item.descricao),
      datasets: [
        {
          backgroundColor: hexToRgba(brandSuccess, 50),
          borderColor: hexToRgba(brandSuccess),
          borderWidth: 1,
          label: "KW/h",
          barPercentage: 0.6,
          data: consumoPorGrupos.map((item) => item.kwh)
        },
        {
          backgroundColor: hexToRgba(brandSuccess, 50),
          borderColor: hexToRgba(brandSuccess),
          borderWidth: 1,
          label: "R$",
          barPercentage: 0.6,
          data: consumoPorGrupos.map((item) => item.valorGasto)
        }
      ],
    };

    return (
      <div className="animated fadeIn">
        <CardBody>
          <div className="chart-wrapper" style={{ height: "350px" }}>
            <HorizontalBar
              data={chartData}
              options={chartOptions}
              height={350}
            />
          </div>
        </CardBody>
      </div>
    );
  }
}

export default consumoPorGrupos;
