import React, { Component } from "react";

import { HorizontalBar } from "react-chartjs-2";
import "chartjs-plugin-labels";

import { CardBody } from "reactstrap";
import { getStyle } from "@coreui/coreui/dist/js/coreui-utilities";

const brandDark = getStyle("--dark");
const colors = ['#32CD32', '#6B8E23', '#A0522D', '#9ACD32', '#008080', '#708090', '#2F4F4F'];

const chartOptions = {
  maintainAspectRatio: false,
  scales: {
    xAxes: [
      {
        ticks: {
          beginAtZero: true
        }
      }
    ],
  },
  legend: {
    display: true

  },
  tooltips: {
    callbacks: {
        label: function(tooltipItems, data) {
            return data.datasets[tooltipItems.datasetIndex].label +': R$ ' + tooltipItems.xLabel;
        }
    }
  },
  plugins: {
    labels: {
      fontSize: 10,
      fontColor: brandDark,
      precision: 1,
      render: function (args) {
        return 'R$ ' + args.value;
      }
    },
  },
};

class consumoGruposPorSetor extends Component {
  render() {
    const { consumoGruposPorSetor } = this.props;
    var datasets = [];
    const grupos = consumoGruposPorSetor && consumoGruposPorSetor[0] && consumoGruposPorSetor[0].grupos;
    grupos.forEach((grupo, index) => {
      datasets.push({
        backgroundColor: colors[index],
        borderWidth: 1,
        label: grupo.grupo,
        barPercentage: 0.6,
        data: consumoGruposPorSetor.map((item) => item[grupo.grupo])
      });
    });

    const chartData = {
      labels: consumoGruposPorSetor.map((item) => item.setor),
      datasets: datasets,
    };

    return (
      <div className="animated fadeIn">
        <CardBody>
          <div className="chart-wrapper" style={{ height: "350px" }}>
            <HorizontalBar
              data={chartData}
              options={chartOptions}
              height={350}
            />
          </div>
        </CardBody>
      </div>
    );
  }
}

export default consumoGruposPorSetor;
