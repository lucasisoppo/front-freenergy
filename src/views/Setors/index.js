import React, { Component } from "react";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import SetorsActions from "../../store/ducks/setors";
import EmpresasActions from "../../store/ducks/empresas";
// import { formatPrice } from "../../util/format";
import {
  EmpresaNotFound,
  SetorsNotFound,
} from "../../components/PagesNotFound";

import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Button,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  Label,
  FormGroup,
  FormFeedback,
} from "reactstrap";
import { Loading } from "../../components/Loading";
import FabButton from "../../styles/components/FabButton";

class Setors extends Component {
  initial_state = {
    id: "",
    descricao: "",
    triedToSubmit: false,
  };

  state = {
    ...this.initial_state,
    modalTitle: "",
    activeEmpresa: null,
  };

  componentDidMount() {
    const { setors, activeEmpresa } = this.props;

    if (activeEmpresa) {
      this.getData(setors.setorCurrentPage);
    }
  }

  getData() {
    const { setors, getSetorsRequest } = this.props;

    getSetorsRequest(
      setors.setorCurrentPage,
      setors.setorPerPage,
      setors.setorFilter
    );
  }

  refreshPage() {
    this.getData();
  }

  async handleFilter(e) {
    const { setSetorFilter, setSetorCurrentPage } = this.props;

    await setSetorFilter(e.target.value);
    await setSetorCurrentPage(1);
    this.getData();
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ triedToSubmit: true });
    const { submitSetorRequest } = this.props;
    const { id, descricao } = this.state;

    submitSetorRequest(id, descricao);
  }

  handleCreate() {
    this.clearFields();
    this.setState({ modalTitle: "Cadastro de setores" });
    this.toggleModal();
  }

  handleEdit(setor) {
    this.setState({
      ...setor,
      modalTitle: "Editando setores",
    });

    this.toggleModal();
  }

  handleDelete(id) {
    this.setState({ id });
    this.toggleModalDelete();
  }

  handleConfirmDelete() {
    const { deleteSetorRequest } = this.props;
    const { id } = this.state;

    deleteSetorRequest(id);
  }

  toggleModal() {
    const { toggleSetorModal } = this.props;
    toggleSetorModal();
  }

  toggleModalDelete() {
    const { toggleSetorDeleteModal } = this.props;
    toggleSetorDeleteModal();
  }

  clearFields() {
    this.setState(this.initial_state);
  }

  cancelSubmit = () => {
    this.clearFields();
    this.toggleModal();
  };

  cancelDelete = () => {
    this.clearFields();
    this.toggleModalDelete();
  };

  async handleSelectPage(page) {
    const { setSetorCurrentPage } = this.props;
    await setSetorCurrentPage(page);
    this.getData();
  }

  render() {
    const { descricao, modalTitle, triedToSubmit } = this.state;

    const { setors, activeEmpresa } = this.props;

    if (activeEmpresa === null) return <EmpresaNotFound />;

    // Cria um array para a paginação
    let pagination = [];
    for (let index = 1; index <= setors.result.lastPage; index++) {
      pagination.push(index);
    }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <span className="float-left pt-1">
                  <i className="fa fa-cogs mr-2"></i> Setores
                </span>
                <span className="float-right ml-2">
                  <div className="button-group-icons-only">
                    <Button
                      style={{ padding: "0 8px" }}
                      onClick={() => this.refreshPage()}
                    >
                      <i className="fa fa-refresh"></i>
                    </Button>
                  </div>
                </span>
                <span className="float-right pt-1">
                  Total de items: {setors.result.total}
                </span>
              </CardHeader>
              <CardBody>
                {/* Campo de pesquisa */}
                <Input
                  type="search"
                  id="filter"
                  name="filter"
                  value={setors.setorFilter}
                  onChange={(e) => this.handleFilter(e)}
                  placeholder="Digite para pesquisar..."
                />

                {setors.isLoading && (
                  <div className="my-5">
                    <Loading color="info" textAlign="text-center" size="md" />
                  </div>
                )}

                {!setors.isLoading && setors.result.data.length === 0 && (
                  <SetorsNotFound />
                )}

                {!setors.isLoading && !(setors.result.data.length === 0) && (
                  <React.Fragment>
                    {/* Tabela */}
                    <Table responsive striped className="mt-3">
                      <thead>
                        <tr>
                          <th>DESCRIÇÃO</th>
                          <th width="5%"></th>
                        </tr>
                      </thead>
                      <tbody>
                        {setors.result.data.map((setor) => (
                          <tr key={setor.id}>
                            <td>{setor.descricao}</td>
                            <td style={{ minWidth: "120px" }}>
                              <div className="button-group-icons-only">
                                <Button
                                  style={{ padding: "0 8px" }}
                                  onClick={() => this.handleEdit(setor)}
                                >
                                  <i className="fa fa-pencil-square-o text-primary"></i>
                                </Button>
                                <Button
                                  style={{ padding: "0 8px" }}
                                  onClick={() => this.handleDelete(setor.id)}
                                >
                                  <i className="fa fa-trash-o text-danger"></i>
                                </Button>
                              </div>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>

                    {/* Paginação */}
                    <Pagination>
                      <PaginationItem disabled={setors.result.page === 1}>
                        <PaginationLink
                          previous
                          tag="button"
                          onClick={() =>
                            this.handleSelectPage(setors.setorCurrentPage - 1)
                          }
                        >
                          Ant.
                        </PaginationLink>
                      </PaginationItem>
                      {pagination.map((page, key) => (
                        <PaginationItem
                          key="key"
                          active={setors.result.page === page}
                        >
                          <PaginationLink
                            tag="button"
                            onClick={() => this.handleSelectPage(page)}
                          >
                            {page}
                          </PaginationLink>
                        </PaginationItem>
                      ))}
                      <PaginationItem
                        disabled={setors.result.page === setors.result.lastPage}
                      >
                        <PaginationLink
                          next
                          tag="button"
                          onClick={() =>
                            this.handleSelectPage(setors.setorCurrentPage + 1)
                          }
                        >
                          Próx.
                        </PaginationLink>
                      </PaginationItem>
                    </Pagination>
                  </React.Fragment>
                )}
              </CardBody>
            </Card>
          </Col>
        </Row>

        {/* Modal */}
        <Modal isOpen={setors.setorModalOpen} toggle={() => this.toggleModal()}>
          <Form onSubmit={(e) => this.handleSubmit(e)}>
            <ModalHeader toggle={() => this.toggleModal()}>
              {modalTitle}
            </ModalHeader>
            <ModalBody>
              <FormGroup>
                <Label for="descricao">DESCRIÇÃO</Label>
                <Input
                  type="text"
                  id="descricao"
                  name="descricao"
                  value={descricao}
                  onChange={(e) => this.setState({ descricao: e.target.value })}
                  placeholder="Descrição do setor"
                  invalid={triedToSubmit && descricao === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
            </ModalBody>
            <ModalFooter>
              <Button color="success" type="submit">
                Salvar
              </Button>{" "}
              <Button color="secondary" onClick={() => this.cancelSubmit()}>
                Cancelar
              </Button>
            </ModalFooter>
          </Form>
        </Modal>

        {/* Modal de Exclusão */}
        <Modal
          isOpen={setors.setorDeleteModalOpen}
          toggle={() => this.toggleModalDelete()}
        >
          <ModalHeader toggle={() => this.toggleModalDelete()}>
            Confirmar
          </ModalHeader>
          <ModalBody>Deseja realmente excluir esse item?</ModalBody>
          <ModalFooter>
            <Button
              color="secondary"
              onClick={() => this.handleConfirmDelete()}
            >
              Sim
            </Button>{" "}
            <Button color="danger" onClick={() => this.cancelDelete()}>
              Não
            </Button>
          </ModalFooter>
        </Modal>

        <FabButton onClick={() => this.handleCreate()}>
          <i className="fa fa-plus"></i>
        </FabButton>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  activeEmpresa: state.empresas.active,
  setors: state.setors,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...EmpresasActions, ...SetorsActions }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Setors);
