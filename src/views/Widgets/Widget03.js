import React, { Component } from "react";
import PropTypes from "prop-types";
import { Card, CardBody, Progress, Row, Col } from "reactstrap";
import classNames from "classnames";
import { mapToCssModules } from "reactstrap/lib/utils";
import { Loading } from "../../components/Loading";

const propTypes = {
  header: PropTypes.string,
  header1: PropTypes.string,
  icon: PropTypes.string,
  color: PropTypes.string,
  value: PropTypes.string,
  children: PropTypes.node,
  className: PropTypes.string,
  cssModule: PropTypes.object,
  invert: PropTypes.bool
};

const defaultProps = {
  header: "0",
  header1: "0",
  icon: "icon-people",
  color: "info",
  value: null,
  children: "Visitors",
  invert: false
};

class Widget04 extends Component {
  render() {
    const {
      className,
      cssModule,
      header,
      header1,
      icon,
      color,
      value,
      children,
      invert,
      ...attributes
    } = this.props;

    // demo purposes only
    const progress = { style: "", color: color, value: value };
    const card = { style: "", bgColor: "", icon: icon };

    if (invert) {
      progress.style = "progress-white";
      progress.color = "";
      card.style = "text-white";
      card.bgColor = "bg-" + color;
    }

    const classes = mapToCssModules(
      classNames(className, card.style, card.bgColor),
      cssModule
    );
    progress.style = classNames("progress-xs mt-3 mb-0", progress.style);

    return (
      <Card className={classes} {...attributes}>
        <CardBody>
          <Row>
            <Col xs="12" sm="6" md="6" lg="6">
              <small className="text-muted text-uppercase font-weight-bold">
                {children}
              </small>
            </Col>
            <Col xs="12" sm="6" md="6" lg="6">
              <div className="h1 text-muted text-right mb-2">
                <i className={card.icon}></i>
              </div>
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="6" md="6" lg="6">
              {!header ? (
                <Loading color="light" align="left" size="md" />
              ) : (
                <div className="h4 mb-0">
                  <Row>
                    <Col xs="12" sm="6" md="6" lg="12">
                      <small
                        style={{ fontSize: 12 }}
                        className="text-muted text-uppercase font-weight-bold"
                      >
                        CASHBACK
                      </small>
                    </Col>
                    <Col xs="12" sm="6" md="6" lg="12">
                      {header}
                    </Col>
                  </Row>
                </div>
              )}
            </Col>
            <Col xs="12" sm="6" md="6" lg="6">
              {!header1 ? (
                <Loading color="light" align="left" size="md" />
              ) : (
                <div className="h4 mb-0">
                  <Row>
                    <Col xs="12" sm="6" md="6" lg="12">
                      <small
                        style={{ fontSize: 12 }}
                        className="text-muted text-uppercase font-weight-bold"
                      >
                        RESGATE
                      </small>
                    </Col>
                    <Col xs="12" sm="6" md="6" lg="12">
                      {header1}
                    </Col>
                  </Row>
                </div>
              )}
            </Col>
          </Row>
          {value && (
            <Progress
              className={progress.style}
              color={progress.color}
              value={progress.value}
            />
          )}
        </CardBody>
      </Card>
    );
  }
}

Widget04.propTypes = propTypes;
Widget04.defaultProps = defaultProps;

export default Widget04;
