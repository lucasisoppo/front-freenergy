import React, { Component } from "react";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import GruposActions from "../../store/ducks/grupos";
import EmpresasActions from "../../store/ducks/empresas";

import {
  EmpresaNotFound,
  GruposNotFound,
} from "../../components/PagesNotFound";

import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Button,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  Label,
  FormGroup,
  FormFeedback,
} from "reactstrap";
import { Loading } from "../../components/Loading";
import FabButton from "../../styles/components/FabButton";

class Grupos extends Component {
  initial_state = {
    id: "",
    descricao: "",
    tensao: "",
    triedToSubmit: false,
  };

  state = {
    ...this.initial_state,
    modalTitle: "",
    activeEmpresa: null,
  };

  componentDidMount() {
    const { grupos, activeEmpresa } = this.props;

    if (activeEmpresa) {
      this.getData(grupos.grupoCurrentPage);
    }
  }

  getData() {
    const { grupos, getGruposRequest } = this.props;

    getGruposRequest(
      grupos.grupoCurrentPage,
      grupos.grupoPerPage,
      grupos.grupoFilter
    );
  }

  refreshPage() {
    this.getData();
  }

  async handleFilter(e) {
    const { setGrupoFilter, setGrupoCurrentPage } = this.props;

    await setGrupoFilter(e.target.value);
    await setGrupoCurrentPage(1);
    this.getData();
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ triedToSubmit: true });
    const { submitGrupoRequest } = this.props;
    const { id, descricao, tensao } = this.state;

    submitGrupoRequest(id, descricao, tensao);
  }

  handleCreate() {
    this.clearFields();
    this.setState({ modalTitle: "Cadastro de grupos" });
    this.toggleModal();
  }

  handleEdit(grupo) {
    this.setState({
      ...grupo,
      modalTitle: "Editando grupo",
    });

    this.toggleModal();
  }

  handleDelete(id) {
    this.setState({ id });
    this.toggleModalDelete();
  }

  handleConfirmDelete() {
    const { deleteGrupoRequest } = this.props;
    const { id } = this.state;

    deleteGrupoRequest(id);
  }

  toggleModal() {
    const { toggleGrupoModal } = this.props;
    toggleGrupoModal();
  }

  toggleModalDelete() {
    const { toggleGrupoDeleteModal } = this.props;
    toggleGrupoDeleteModal();
  }

  clearFields() {
    this.setState(this.initial_state);
  }

  cancelSubmit = () => {
    this.clearFields();
    this.toggleModal();
  };

  cancelDelete = () => {
    this.clearFields();
    this.toggleModalDelete();
  };

  async handleSelectPage(page) {
    const { setGrupoCurrentPage } = this.props;
    await setGrupoCurrentPage(page);
    this.getData();
  }

  render() {
    const {
      descricao,
      tensao,
      modalTitle,
      triedToSubmit,
    } = this.state;

    const { grupos, activeEmpresa } = this.props;

    if (activeEmpresa === null) return <EmpresaNotFound />;

    // Cria um array para a paginação
    let pagination = [];
    for (let index = 1; index <= grupos.result.lastPage; index++) {
      pagination.push(index);
    }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <span className="float-left pt-1">
                  <i className="fa fa-cogs mr-2"></i> Grupos
                </span>
                <span className="float-right ml-2">
                  <div className="button-group-icons-only">
                    <Button
                      style={{ padding: "0 8px" }}
                      onClick={() => this.refreshPage()}
                    >
                      <i className="fa fa-refresh"></i>
                    </Button>
                  </div>
                </span>
                <span className="float-right pt-1">
                  Total de items: {grupos.result.total}
                </span>
              </CardHeader>
              <CardBody>
                {/* Campo de pesquisa */}
                <Input
                  type="search"
                  id="filter"
                  name="filter"
                  value={grupos.grupoFilter}
                  onChange={(e) => this.handleFilter(e)}
                  placeholder="Digite para pesquisar..."
                />

                {grupos.isLoading && (
                  <div className="my-5">
                    <Loading color="info" textAlign="text-center" size="md" />
                  </div>
                )}

                {!grupos.isLoading && grupos.result.data.length === 0 && (
                  <GruposNotFound />
                )}

                {!grupos.isLoading && !(grupos.result.data.length === 0) && (
                  <React.Fragment>
                    {/* Tabela */}
                    <Table responsive striped className="mt-3">
                      <thead>
                        <tr>
                          <th>DESCRIÇÃO</th>
                          <th>TENSÃO</th>
                          <th width="5%"></th>
                        </tr>
                      </thead>
                      <tbody>
                        {grupos.result.data.map((grupo) => (
                          <tr key={grupo.id}>
                            <td>{grupo.descricao}</td>
                            <td>{grupo.tensao} volts</td>
                            <td style={{ minWidth: "120px" }}>
                              <div className="button-group-icons-only">
                                <Button
                                  style={{ padding: "0 8px" }}
                                  onClick={() => this.handleEdit(grupo)}
                                >
                                  <i className="fa fa-pencil-square-o text-primary"></i>
                                </Button>
                                <Button
                                  style={{ padding: "0 8px" }}
                                  onClick={() => this.handleDelete(grupo.id)}
                                >
                                  <i className="fa fa-trash-o text-danger"></i>
                                </Button>
                              </div>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>

                    {/* Paginação */}
                    <Pagination>
                      <PaginationItem disabled={grupos.result.page === 1}>
                        <PaginationLink
                          previous
                          tag="button"
                          onClick={() =>
                            this.handleSelectPage(grupos.grupoCurrentPage - 1)
                          }
                        >
                          Ant.
                        </PaginationLink>
                      </PaginationItem>
                      {pagination.map((page, key) => (
                        <PaginationItem
                          key="key"
                          active={grupos.result.page === page}
                        >
                          <PaginationLink
                            tag="button"
                            onClick={() => this.handleSelectPage(page)}
                          >
                            {page}
                          </PaginationLink>
                        </PaginationItem>
                      ))}
                      <PaginationItem
                        disabled={grupos.result.page === grupos.result.lastPage}
                      >
                        <PaginationLink
                          next
                          tag="button"
                          onClick={() =>
                            this.handleSelectPage(grupos.grupoCurrentPage + 1)
                          }
                        >
                          Próx.
                        </PaginationLink>
                      </PaginationItem>
                    </Pagination>
                  </React.Fragment>
                )}
              </CardBody>
            </Card>
          </Col>
        </Row>

        {/* Modal */}
        <Modal isOpen={grupos.grupoModalOpen} toggle={() => this.toggleModal()}>
          <Form onSubmit={(e) => this.handleSubmit(e)}>
            <ModalHeader toggle={() => this.toggleModal()}>
              {modalTitle}
            </ModalHeader>
            <ModalBody>
              <FormGroup>
                <Label for="descricao">DESCRIÇÃO</Label>
                <Input
                  type="text"
                  id="descricao"
                  name="descricao"
                  value={descricao}
                  onChange={(e) => this.setState({ descricao: e.target.value })}
                  placeholder="Descrição do grupo"
                  invalid={triedToSubmit && descricao === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
              {/* </FormGroup> */}
              <FormGroup>
                <Label for="tensao">TENSÃO</Label>
                <Input
                  type="tensao"
                  id="tensao"
                  name="tensao"
                  value={tensao}
                  onChange={(e) => this.setState({ tensao: e.target.value })}
                  placeholder="220 volts"
                />
              </FormGroup>
            </ModalBody>
            <ModalFooter>
              <Button color="success" type="submit">
                Salvar
              </Button>{" "}
              <Button color="secondary" onClick={() => this.cancelSubmit()}>
                Cancelar
              </Button>
            </ModalFooter>
          </Form>
        </Modal>

        {/* Modal de Exclusão */}
        <Modal
          isOpen={grupos.grupoDeleteModalOpen}
          toggle={() => this.toggleModalDelete()}
        >
          <ModalHeader toggle={() => this.toggleModalDelete()}>
            Confirmar
          </ModalHeader>
          <ModalBody>Deseja realmente excluir esse item?</ModalBody>
          <ModalFooter>
            <Button
              color="secondary"
              onClick={() => this.handleConfirmDelete()}
            >
              Sim
            </Button>{" "}
            <Button color="danger" onClick={() => this.cancelDelete()}>
              Não
            </Button>
          </ModalFooter>
        </Modal>

        <FabButton onClick={() => this.handleCreate()}>
          <i className="fa fa-plus"></i>
        </FabButton>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  activeEmpresa: state.empresas.active,
  grupos: state.grupos,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...EmpresasActions, ...GruposActions }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Grupos);
