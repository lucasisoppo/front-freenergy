import React, { Component } from "react";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import EmpresasActions from "../../store/ducks/empresas";
import {
  EmpresaNotFound,
  GruposNotFound
} from "../../components/PagesNotFound";

import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row,
  Table,
  Button,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  Label,
  FormGroup,
  FormFeedback
} from "reactstrap";
import { Loading } from "../../components/Loading";
import FabButton from "../../styles/components/FabButton";
import { cpfMask, foneCelMask } from "../../util/mask";
import { formatDate } from "../../util/format";

class Alertas extends Component {
  initial_state = {
    id: "",
    nome: "",
    cpf: "",
    telefone: "",
    email: "",
    triedToSubmit: false
  };

  state = {
    ...this.initial_state,
    modalTitle: "",
    activeEmpresa: null
  };

  componentDidMount() {
    const { clientes, activeEmpresa } = this.props;

    if (activeEmpresa) {
      this.getData(clientes.clienteCurrentPage);
    }
  }

  getData() {
    const { clientes, getClientesRequest } = this.props;

    getClientesRequest(
      clientes.clienteCurrentPage,
      clientes.clientePerPage,
      clientes.clienteFilter
    );
  }

  refreshPage() {
    this.getData();
  }

  async handleFilter(e) {
    const { setClienteFilter, setClienteCurrentPage } = this.props;

    await setClienteFilter(e.target.value);
    await setClienteCurrentPage(1);
    this.getData();
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ triedToSubmit: true });
    const { submitClienteRequest } = this.props;
    const { id, nome, cpf, telefone, email } = this.state;

    submitClienteRequest(id, nome, cpf, telefone, email);
  }

  handleCreate() {
    this.clearFields();
    this.setState({ modalTitle: "Cadastro de cliente" });
    this.toggleModal();
  }

  handleEdit(cliente) {
    this.setState({
      ...cliente,
      modalTitle: "Editando cliente"
    });

    this.toggleModal();
  }

  handleDelete(id) {
    this.setState({ id });
    this.toggleModalDelete();
  }

  handleConfirmDelete() {
    const { deleteClienteRequest } = this.props;
    const { id } = this.state;

    deleteClienteRequest(id);
  }

  toggleModal() {
    const { toggleClienteModal } = this.props;
    toggleClienteModal();
  }

  toggleModalDelete() {
    const { toggleClienteDeleteModal } = this.props;
    toggleClienteDeleteModal();
  }

  clearFields() {
    this.setState(this.initial_state);
  }

  cancelSubmit = () => {
    this.clearFields();
    this.toggleModal();
  };

  cancelDelete = () => {
    this.clearFields();
    this.toggleModalDelete();
  };

  async handleSelectPage(page) {
    const { setClienteCurrentPage } = this.props;
    await setClienteCurrentPage(page);
    this.getData();
  }

  render() {
    const {
      nome,
      cpf,
      telefone,
      email,
      modalTitle,
      triedToSubmit
    } = this.state;

    const { clientes, activeEmpresa } = this.props;

    if (activeEmpresa === null) return <EmpresaNotFound />;

    // Cria um array para a paginação
    let pagination = [];
    for (let index = 1; index <= clientes.result.lastPage; index++) {
      pagination.push(index);
    }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <span className="float-left pt-1">
                  <i className="fa fa-users mr-2"></i> Alertas
                </span>
                <span className="float-right ml-2">
                  <div className="button-group-icons-only">
                    <Button
                      style={{ padding: "0 8px" }}
                      onClick={() => this.refreshPage()}
                    >
                      <i className="fa fa-refresh"></i>
                    </Button>
                  </div>
                </span>
                <span className="float-right pt-1">
                  Total de items: {clientes.result.total}
                </span>
              </CardHeader>
              <CardBody>
                {/* Campo de pesquisa */}
                <Input
                  type="search"
                  id="filter"
                  name="filter"
                  value={clientes.clienteFilter}
                  onChange={e => this.handleFilter(e)}
                  placeholder="Digite para pesquisar..."
                />

                {clientes.isLoading && (
                  <div className="my-5">
                    <Loading color="info" textAlign="text-center" size="md" />
                  </div>
                )}

                {!clientes.isLoading && clientes.result.data.length === 0 && (
                  <GruposNotFound />
                )}

                {!clientes.isLoading && !(clientes.result.data.length === 0) && (
                  <React.Fragment>
                    {/* Tabela */}
                    <Table responsive striped className="mt-3">
                      <thead>
                        <tr>
                          <th>NOME</th>
                          <th>CPF</th>
                          <th>TELEFONE</th>
                          <th>E-MAIL</th>
                          <th>DATA</th>
                          <th width="5%"></th>
                        </tr>
                      </thead>
                      <tbody>
                        {clientes.result.data.map(cliente => (
                          <tr key={cliente.id}>
                            <td>{cliente.nome}</td>
                            <td>{cliente.cpf}</td>
                            <td>{cliente.telefone}</td>
                            <td>{cliente.email}</td>
                            <td>{formatDate(cliente.updated_at)}</td>
                            <td style={{ minWidth: "120px" }}>
                              <div className="button-group-icons-only">
                                <Button
                                  style={{ padding: "0 8px" }}
                                  onClick={() => this.handleEdit(cliente)}
                                >
                                  <i className="fa fa-pencil-square-o text-primary"></i>
                                </Button>
                                <Button
                                  style={{ padding: "0 8px" }}
                                  onClick={() => this.handleDelete(cliente.id)}
                                >
                                  <i className="fa fa-trash-o text-danger"></i>
                                </Button>
                              </div>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>

                    {/* Paginação */}
                    <Pagination>
                      <PaginationItem disabled={clientes.result.page === 1}>
                        <PaginationLink
                          previous
                          tag="button"
                          onClick={() =>
                            this.handleSelectPage(
                              clientes.clienteCurrentPage - 1
                            )
                          }
                        >
                          Ant.
                        </PaginationLink>
                      </PaginationItem>
                      {pagination.map((page, key) => (
                        <PaginationItem
                          key="key"
                          active={clientes.result.page === page}
                        >
                          <PaginationLink
                            tag="button"
                            onClick={() => this.handleSelectPage(page)}
                          >
                            {page}
                          </PaginationLink>
                        </PaginationItem>
                      ))}
                      <PaginationItem
                        disabled={
                          clientes.result.page === clientes.result.lastPage
                        }
                      >
                        <PaginationLink
                          next
                          tag="button"
                          onClick={() =>
                            this.handleSelectPage(
                              clientes.clienteCurrentPage + 1
                            )
                          }
                        >
                          Próx.
                        </PaginationLink>
                      </PaginationItem>
                    </Pagination>
                  </React.Fragment>
                )}
              </CardBody>
            </Card>
          </Col>
        </Row>

        {/* Modal */}
        <Modal
          isOpen={clientes.clienteModalOpen}
          toggle={() => this.toggleModal()}
        >
          <Form onSubmit={e => this.handleSubmit(e)}>
            <ModalHeader toggle={() => this.toggleModal()}>
              {modalTitle}
            </ModalHeader>
            <ModalBody>
              <FormGroup>
                <Label for="nome">Nome</Label>
                <Input
                  type="text"
                  id="nome"
                  name="nome"
                  value={nome}
                  onChange={e => this.setState({ nome: e.target.value })}
                  placeholder="Nome completo"
                  invalid={triedToSubmit && nome === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label for="cpf">CPF</Label>
                <Input
                  type="text"
                  id="cpf"
                  name="cpf"
                  value={cpf}
                  onChange={e =>
                    this.setState({ cpf: cpfMask(e.target.value) })
                  }
                  placeholder="000.000.000-00"
                  maxLength="14"
                  invalid={triedToSubmit && cpf === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label for="telefone"> Telefone celular</Label>
                <Input
                  type="text"
                  id="telefone"
                  name="telefone"
                  value={telefone}
                  onChange={e =>
                    this.setState({ telefone: foneCelMask(e.target.value) })
                  }
                  placeholder="(DD) 99999-9999"
                />
              </FormGroup>
              {/* </FormGroup> */}
              <FormGroup>
                <Label for="email">E-mail</Label>
                <Input
                  type="email"
                  id="email"
                  name="email"
                  value={email}
                  onChange={e => this.setState({ email: e.target.value })}
                  placeholder="exemplo@exemplo.com"
                />
              </FormGroup>
            </ModalBody>
            <ModalFooter>
              <Button color="success" type="submit">
                Salvar
              </Button>{" "}
              <Button color="secondary" onClick={() => this.cancelSubmit()}>
                Cancelar
              </Button>
            </ModalFooter>
          </Form>
        </Modal>

        {/* Modal de Exclusão */}
        <Modal
          isOpen={clientes.clienteDeleteModalOpen}
          toggle={() => this.toggleModalDelete()}
        >
          <ModalHeader toggle={() => this.toggleModalDelete()}>
            Confirmar
          </ModalHeader>
          <ModalBody>Deseja realmente excluir esse item?</ModalBody>
          <ModalFooter>
            <Button
              color="secondary"
              onClick={() => this.handleConfirmDelete()}
            >
              Sim
            </Button>{" "}
            <Button color="danger" onClick={() => this.cancelDelete()}>
              Não
            </Button>
          </ModalFooter>
        </Modal>

        <FabButton onClick={() => this.handleCreate()}>
          <i className="fa fa-plus"></i>
        </FabButton>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  activeEmpresa: state.empresas.active,
  clientes: state.clientes
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...EmpresasActions }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Alertas);
