import React, { Component } from "react";

import Can from "../../components/Can";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import EmpresasActions from "../../store/ducks/empresas";
import { EmpresaNotFound, EmpresasNotFound } from "../../components/PagesNotFound";
import Api from "../../services/api";

import {
  Card,
  CardBody,
  CardHeader,
  Pagination,
  PaginationItem,
  PaginationLink,
  Col,
  Row,
  Table,
  Button,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  Label,
  FormGroup,
  FormFeedback
} from "reactstrap";
import Select from "react-select";
import { Loading } from "../../components/Loading";
import FabButton from "../../styles/components/FabButton";
import { cnpjMask, cepMask, foneMask, foneCelMask } from "../../util/mask";

class Empresas extends Component {
  initial_state = {
    id: "",
    nome: "",
    cnpj: "",
    endereco: "",
    cidade: "",
    uf: "",
    cep: "",
    telefone: "",
    celular: "",
    email: "",
    selected_concessionaria: ""
  };

  state = {
    ...this.initial_state,
    modalTitle: "",
    concessionarias: [],
    tarifa_branca:false,
    activeEmpresa: null
  };

  async componentDidMount() {
    const { empresas, activeEmpresa } = this.props;

    if (activeEmpresa) {
      this.getData(empresas.empresaCurrentPage);
      await this.getDataConcessionarias();
    }
  }

  getData() {
    const { empresas, getEmpresasRequest } = this.props;

    getEmpresasRequest(
      empresas.empresaCurrentPage,
      empresas.empresaPerPage,
      empresas.empresaFilter
    );
  }

  async getDataConcessionarias() {
    const res = await Api.get("/concessionarias");

    this.setState({ concessionarias: res.data });
  }

  refreshPage() {
    this.getData();
  }

  async handleFilter(e) {
    const { setEmpresaFilter, setEmpresaCurrentPage } = this.props;

    await setEmpresaFilter(e.target.value);
    await setEmpresaCurrentPage(1);
    this.getData();
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ triedToSubmit: true });
    const { submitEmpresaRequest } = this.props;
    const {
      id,
      nome,
      cnpj,
      endereco,
      cidade,
      uf,
      cep,
      telefone,
      celular,
      email,
      selected_concessionaria,
      tarifa_branca
    } = this.state;

    if (!selected_concessionaria) {
      return false;
    }

    submitEmpresaRequest(
      id,
      nome,
      cnpj,
      endereco,
      cidade,
      uf,
      cep,
      telefone,
      celular,
      email,
      +selected_concessionaria.id,
      tarifa_branca
    );
  }

  handleEmpresaSelect = empresa => {
    const { selectEmpresa } = this.props;

    selectEmpresa(empresa);
  };

  handleCreate() {
    this.clearFields();
    this.setState({ modalTitle: "Cadastro de empresa" });
    this.toggleModal();
  }

  handleEdit(empresa) {
    this.setState({
      ...empresa,
      selected_concessionaria: empresa.concessionaria,
      modalTitle: "Editando empresa"
    });

    this.toggleModal();
  }

  handleDelete(id) {
    this.setState({ id });
    this.toggleModalDelete();
  }

  handleConfirmDelete() {
    const { deleteEmpresaRequest } = this.props;
    const { id } = this.state;

    deleteEmpresaRequest(id);
  }

  toggleModal() {
    const { toggleEmpresaModal } = this.props;
    toggleEmpresaModal();
  }

  toggleModalDelete() {
    const { toggleEmpresaDeleteModal } = this.props;
    toggleEmpresaDeleteModal();
  }

  clearFields() {
    this.setState(this.initial_state);
  }

  cancelSubmit = () => {
    this.clearFields();
    this.toggleModal();
  };

  cancelDelete = () => {
    this.clearFields();
    this.toggleModalDelete();
  };

  async handleSelectPage(page) {
    const { setEmpresaCurrentPage } = this.props;
    await setEmpresaCurrentPage(page);
    this.getData();
  }
  render() {
    const {
      nome,
      cnpj,
      endereco,
      cidade,
      uf,
      cep,
      telefone,
      celular,
      email,
      concessionarias,
      selected_concessionaria,
      tarifa_branca,
      modalTitle,
      triedToSubmit
    } = this.state;

    const { empresas, activeEmpresa } = this.props;

    if (activeEmpresa === null) return <EmpresaNotFound />;

    let pagination = [];
    for (let index = 1; index <= empresas.result.lastPage; index++) {
      pagination.push(index);
    }

    // Cria um array para a paginação
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <span className="float-left pt-1">
                  <i className="fa fa-building-o mr-2"></i> Empresas
                </span>
                <span className="float-right ml-2">
                  <div className="button-group-icons-only">
                    <Button
                      style={{ padding: "0 8px" }}
                      onClick={() => this.refreshPage()}
                    >
                      <i className="fa fa-refresh"></i>
                    </Button>
                  </div>
                </span>
                <span className="float-right pt-1">
                  Total de items: {empresas.result.total}
                </span>
              </CardHeader>
              <CardBody>
                {/* Campo de pesquisa */}
                <Input
                  type="search"
                  id="filter"
                  name="filter"
                  value={empresas.empresaFilter}
                  onChange={e => this.handleFilter(e)}
                  placeholder="Digite para pesquisar..."
                />
                {empresas.isLoading && (
                  <div className="my-5">
                    <Loading color="info" textAlign="text-center" size="md" />
                  </div>
                )}

                {!empresas.isLoading && empresas.result.data.length === 0 && (
                  <EmpresasNotFound />
                )}

                {!empresas.isLoading && !(empresas.result.data.length === 0) && (
                  <React.Fragment>
                    {/* Tabela */}
                    <Table responsive striped className="mt-3">
                      <thead>
                        <tr>
                          <th>RAZÃO SOCIAL</th>
                          <th>CNPJ</th>
                          <th>ENDEREÇO</th>
                          <th>CIDADE</th>
                          <th>UF</th>
                          <th>E-MAIL</th>
                          <th width="5%"></th>
                        </tr>
                      </thead>
                      <tbody>
                        {empresas.result.data.map(empresa => (
                          <tr key={empresa.id}>
                            <td>{empresa.nome}</td>
                            <td>{empresa.cnpj}</td>
                            <td>{empresa.endereco}</td>
                            <td>{empresa.cidade}</td>
                            <td>{empresa.uf}</td>
                            <td>{empresa.email}</td>
                            <td style={{ minWidth: "120px" }}>
                              <div className="button-group-icons-only">
                                <Can checkRole="admin">
                                  <Button
                                    style={{ padding: "0 8px" }}
                                    onClick={() => this.handleEdit(empresa)}
                                  >
                                    <i className="fa fa-pencil-square-o text-primary"></i>
                                  </Button>
                                </Can>
                                <Can checkRole="admin">
                                  <Button
                                    style={{ padding: "0 8px" }}
                                    onClick={() =>
                                      this.handleDelete(empresa.id)
                                    }
                                  >
                                    <i className="fa fa-trash-o text-danger"></i>
                                  </Button>
                                </Can>
                              </div>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                    {/* Paginação */}
                    <Pagination>
                      <PaginationItem disabled={empresas.result.page === 1}>
                        <PaginationLink
                          previous
                          tag="button"
                          onClick={() =>
                            this.handleSelectPage(
                              empresas.empresaCurrentPage - 1
                            )
                          }
                        >
                          Ant.
                        </PaginationLink>
                      </PaginationItem>
                      {pagination.map((page, key) => (
                        <PaginationItem
                          key="key"
                          active={empresas.result.page === page}
                        >
                          <PaginationLink
                            tag="button"
                            onClick={() => this.handleSelectPage(page)}
                          >
                            {page}
                          </PaginationLink>
                        </PaginationItem>
                      ))}
                      <PaginationItem
                        disabled={
                          empresas.result.page === empresas.result.lastPage
                        }
                      >
                        <PaginationLink
                          next
                          tag="button"
                          onClick={() =>
                            this.handleSelectPage(
                              empresas.empresaCurrentPage + 1
                            )
                          }
                        >
                          Próx.
                        </PaginationLink>
                      </PaginationItem>
                    </Pagination>
                  </React.Fragment>
                )}
              </CardBody>
            </Card>
          </Col>
        </Row>
        {/* Modal */}
        <Modal
          isOpen={empresas.empresaModalOpen}
          toggle={() => this.toggleModal()}
        >
          <Form onSubmit={e => this.handleSubmit(e)}>
            <ModalHeader toggle={() => this.toggleModal()}>
              {modalTitle}
            </ModalHeader>
            <ModalBody>
              <FormGroup>
                <Label for="nome">Razão social</Label>
                <Input
                  type="text"
                  id="nome"
                  name="nome"
                  value={nome}
                  onChange={e => this.setState({ nome: e.target.value })}
                  placeholder="Empresa exemplo LTDA."
                  invalid={triedToSubmit && nome === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>

              <FormGroup>
                <Label for="cnpj">CNPJ</Label>
                <Input
                  type="text"
                  id="cnpj"
                  name="cnpj"
                  value={cnpj}
                  onChange={e =>
                    this.setState({ cnpj: cnpjMask(e.target.value) })
                  }
                  placeholder="00.000.000/0000-00"
                  invalid={triedToSubmit && cnpj === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
              <Row>
                <Col xs="9">
                  <FormGroup>
                    <Label for="endereco">Endereço</Label>
                    <Input
                      type="text"
                      id="endereco"
                      name="endereco"
                      value={endereco}
                      onChange={e =>
                        this.setState({ endereco: e.target.value })
                      }
                      placeholder="R. Cel. Pedro Benedet, 333 - Centro"
                      invalid={triedToSubmit && endereco === ""}
                    />
                    <FormFeedback>Campo obrigatório</FormFeedback>
                  </FormGroup>
                </Col>
                <Col xs="3">
                  <FormGroup>
                    <Label for="cep">CEP</Label>
                    <Input
                      type="text"
                      id="cep"
                      name="cep"
                      value={cep}
                      onChange={e =>
                        this.setState({ cep: cepMask(e.target.value) })
                      }
                      placeholder="88801-000"
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Col xs="6">
                  <FormGroup>
                    <Label for="cidade">Município</Label>
                    <Input
                      type="text"
                      id="cidade"
                      name="cidade"
                      value={cidade}
                      onChange={e => this.setState({ cidade: e.target.value })}
                      placeholder="Criciúma"
                      invalid={triedToSubmit && cidade === ""}
                    />
                    <FormFeedback>Campo obrigatório</FormFeedback>
                  </FormGroup>
                </Col>
                <Col xs="6">
                  <FormGroup>
                    <Label htmlFor="uf">UF</Label>
                    <Input
                      type="select"
                      id="uf"
                      name="uf"
                      value={uf}
                      onChange={e => this.setState({ uf: e.target.value })}
                      invalid={triedToSubmit && uf === ""}
                    >
                      <option>Selecione um estado</option>
                      <option> AC - Acre</option>
                      <option> AL - Alagoas</option>
                      <option> AP - Amapá</option>
                      <option> AM - Amazonas</option>
                      <option> BA - Bahia</option>
                      <option> CE - Ceará</option>
                      <option> DF - Distrito Federal</option>
                      <option> ES - Espírito Santo </option>
                      <option> GO - Goiás</option>
                      <option> MA - Maranhão</option>
                      <option> MT - Mato Grosso</option>
                      <option> MS - Mato Grosso do sul </option>
                      <option> MG - Minas Gerais </option>
                      <option> PA - Pará</option>
                      <option> PB - Paraíba</option>
                      <option> PR - Paraná</option>
                      <option> PE - Pernambuco</option>
                      <option> PI - Piauí</option>
                      <option> RJ - Rio de Janeiro </option>
                      <option> RN - Rio Grande do Norte </option>
                      <option> RS - Rio Grande do Sul </option>
                      <option> RO - Rondônia</option>
                      <option> RR - Roraima</option>
                      <option> SC - Santa Catarina </option>
                      <option> SP - São Paulo </option>
                      <option> SE - Sergipe</option>
                      <option> TO - Tocantins</option>
                    </Input>
                    <FormFeedback>Campo obrigatório</FormFeedback>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col xs="6">
                  <FormGroup>
                    <Label for="telefone">Telefone residencial</Label>
                    <Input
                      type="text"
                      id="telefone"
                      name="telefone"
                      value={telefone}
                      onChange={e =>
                        this.setState({ telefone: foneMask(e.target.value) })
                      }
                      placeholder="(DD) 4444-4444"
                    />
                  </FormGroup>
                </Col>
                <Col xs="6">
                  <FormGroup>
                    <Label for="celular">Telefone celular</Label>
                    <Input
                      type="text"
                      id="celular"
                      name="celular"
                      value={celular}
                      onChange={e =>
                        this.setState({ celular: foneCelMask(e.target.value) })
                      }
                      placeholder="(DD) 99999-9999"
                    />
                  </FormGroup>
                </Col>
              </Row>
              <FormGroup>
                <Label for="email">E-mail</Label>
                <Input
                  type="email"
                  id="email"
                  name="email"
                  value={email}
                  onChange={e => this.setState({ email: e.target.value })}
                  placeholder="exemplo@exemplo.com"
                  invalid={triedToSubmit && email === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
              <Row className="align-items-center">
                <Col xs="8">
                  <FormGroup>
                    <Label>Concessionária de energia</Label>
                    <Select
                      isClearable={true}
                      isSearchable={true}
                      options={concessionarias}
                      value={selected_concessionaria}
                      getOptionLabel={(concessionaria) => concessionaria.distribuidora}
                      getOptionValue={(concessionaria) => concessionaria.id}
                      onChange={(concessionaria) => this.setState({ selected_concessionaria: concessionaria })}
                      placeholder="Selecione a concessionária"
                    />
                  </FormGroup>
                </Col>
                <Col xs="4">
                  <FormGroup check>
                    <Label check>
                      <Input 
                       type="checkbox" 
                       id="tarifa_branca"
                       name="tarifa_branca"
                       value={tarifa_branca}
                       checked={this.state.tarifa_branca}
                       defaultChecked={this.state.tarifa_branca} 
                       onChange={() => {this.setState({ tarifa_branca: !this.state.tarifa_branca })}}
                     />
                      Tarifa Branca
                    </Label>
                  </FormGroup>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="success" type="submit">
                Salvar
              </Button>
              <Button color="secondary" onClick={() => this.cancelSubmit()}>
                Cancelar
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
        {/* Modal de Exclusão */}
        <Modal
          isOpen={empresas.empresaDeleteModalOpen}
          toggle={() => this.toggleModalDelete()}
        >
          <ModalHeader toggle={() => this.toggleModalDelete()}>
            Confirmar
          </ModalHeader>
          <ModalBody>Deseja realmente excluir esse item?</ModalBody>
          <ModalFooter>
            <Button
              color="secondary"
              onClick={() => this.handleConfirmDelete()}
            >
              Sim
            </Button>{" "}
            <Button color="danger" onClick={() => this.cancelDelete()}>
              Não
            </Button>
          </ModalFooter>
        </Modal>
        <Can checkRole="admin">
          <FabButton onClick={() => this.handleCreate()}>
            <i className="fa fa-plus"></i>
          </FabButton>
        </Can>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  activeEmpresa: state.empresas.active,
  empresas: state.empresas
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(EmpresasActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Empresas);
