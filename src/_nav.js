export default {
  items: [
    {
      title: true,
      name: "Início",
      wrapper: {
        // optional wrapper object
        element: "", // required valid HTML5 element tag
        attributes: {}, // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: "", // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: "Dashboards",
      url: "/dashboard",
      icon: "fa fa-line-chart",
    },
    {
      title: true,
      name: "Menu",
      wrapper: {
        // optional wrapper object
        element: "", // required valid HTML5 element tag
        attributes: {}, // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: "", // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: "Consumos",
      url: "/consumos",
      icon: "fa fa-align-left",
      badge: {
        variant: "success",
        text: "TI VERDE",
      },
    },
    {
      name: "Equipamentos",
      url: "/equipamentos",
      icon: "fa fa-cog",
    },
    {
      name: "Grupos",
      url: "/grupos",
      icon: "fa fa-cogs",
    },
    {
      name: "Setores",
      url: "/setors",
      icon: "fa fa-cogs",
    },
    {
      name: "Empresas",
      url: "/empresas",
      icon: "fa fa-building-o",
    },
  ],
};
