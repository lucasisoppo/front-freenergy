import axios from 'axios';
import store from '../store';

const api = axios.create({
  // baseURL: 'http://localhost:3333',
  baseURL: 'https://api-freenergy.herokuapp.com',

});

api.interceptors.request.use((config) => {
  const { token } = store.getState().auth;
  const { active: empresa } = store.getState().empresas;

  const headers = { ...config.headers };

  if (token) {
    headers.Authorization = `Bearer ${token}`;
  }

  if (empresa) {
    headers.EMPRESA = empresa;
  }

  return { ...config, headers };
});

export default api;
