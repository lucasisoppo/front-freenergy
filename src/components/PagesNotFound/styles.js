import styled from 'styled-components';

export const NotFound = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: #9d9b9b;
  padding-top: 50px;
  padding-bottom: 180px;
  text-align: center;

  h1 {
    font-size: 22px;
    font-weight: 600;
    margin: 10px 30px 0;
  }
  h5 {
    font-size: 16px;
    font-weight: 530;
    margin: 10px 30px 0;
  }
`;
