import React from "react";

import { NotFound } from "./styles";
import { IoIosBusiness } from "react-icons/io";
import {
  FaShoppingBag,
  FaLongArrowAltDown,
  FaTags,
  FaCogs,
  FaCog,
  FaDollarSign,
  FaChartLine,
} from "react-icons/fa";

export function EmpresaNotFound() {
  return (
    <NotFound>
      <br />
      <br />
      <br />
      <IoIosBusiness size={200} />
      <h1>Nenhuma empresa selecionada.</h1>
      <h5>Selecione uma empresa no menu superior.</h5>
    </NotFound>
  );
}

export function EmpresasNotFound() {
  return (
    <NotFound>
      <br />
      <br />
      <br />
      <IoIosBusiness size={200} />
      <h1>Nenhuma empresa encontrada.</h1>
    </NotFound>
  );
}

export function GruposNotFound() {
  return (
    <NotFound>
      <FaCogs size={200} />
      <h1>Nenhum grupo encontrado.</h1>
    </NotFound>
  );
}

export function SetorsNotFound() {
  return (
    <NotFound>
      <FaCogs size={200} />
      <h1>Nenhum setor encontrado.</h1>
    </NotFound>
  );
}

export function EquipamentosNotFound() {
  return (
    <NotFound>
      <FaCog size={200} />
      <h1>Nenhum equipamento encontrado.</h1>
    </NotFound>
  );
}

export function ConsumosNotFound() {
  return (
    <NotFound>
      <FaCog size={200} />
      <h1>Nenhum consumo encontrado.</h1>
    </NotFound>
  );
}

export function CashbackNotFound() {
  return (
    <NotFound>
      <FaTags size={200} />
      <h1>Nenhum cashback encontrado.</h1>
    </NotFound>
  );
}

export function ResgateNotFound() {
  return (
    <NotFound>
      <FaDollarSign size={200} />
      <h1>Nenhum resgate encontrado.</h1>
    </NotFound>
  );
}

export function ConfigProdutoServicoNotFound() {
  return (
    <NotFound>
      <FaShoppingBag size={200} />
      <h1>Nenhum produto ou serviço encontrado.</h1>
    </NotFound>
  );
}

export function ConfigResgateMinimoNotFound() {
  return (
    <NotFound>
      <FaLongArrowAltDown size={200} />
      <h1>Nenhuma resgate minímo encontrado.</h1>
    </NotFound>
  );
}

export function CashbackPorResgateNotFound() {
  return (
    <NotFound>
      <FaChartLine size={200} />
      <h1>Nenhum cashback ou resgate registrado neste mês.</h1>
    </NotFound>
  );
}
