import React, { Component } from 'react';
import { Row, Col, Card, CardHeader, CardTitle } from 'reactstrap';

class Loading extends Component {
  render() {
    const { children, title, subtitle } = this.props;

    return (
      <div className="animated fadeIn">
        <Card>
          <CardHeader className="bg-white border-0">
            <Row>
              <Col>
                <CardTitle className="mb-0">{title}</CardTitle>
                <div className="small text-muted">{subtitle}</div>
              </Col>
            </Row>
          </CardHeader>
          {children}
        </Card>
      </div>
    );
  }
}

export default Loading;