import React, { Component } from "react";

class ListNotFound extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <div className="d-flex align-items-center justify-content-center text-center font-weight-bold py-5">
          <div className="w-50 mx-auto">
            <i
              style={{ fontSize: this.props.fontSize }}
              className={"text-primary " + this.props.icon}
            ></i>
            <div style={{ color: "#2e0091" }} className="mt-3">
              {this.props.text}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ListNotFound;
