import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Spinner } from 'reactstrap';

class Loading extends Component {
  static propTypes = {
    textAlign: PropTypes.string,
    type: PropTypes.string,
    size: PropTypes.string,
    color: PropTypes.string,
  };

  static defaultProps = {
    align: 'center',
    type: 'border',
    size: 'md',
    color: 'primary',
  };

  render() {
    const { align, type, size, color } = this.props;

    return (
      <div className="animated fadeIn" style={{ display: 'flex', alignItems: 'center', justifyContent: align, height: '100%' }
}>
  <Spinner
    type={type}
    size={size}
    color={color}
  />
      </div >
    );
  }
}

export { Loading };