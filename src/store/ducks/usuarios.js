import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

const { Types, Creators } = createActions({
  submitUsuarioRequest: ['nome', 'email', 'cpf'],
  submitUsuarioSuccess: ['data'],
  submitAlterarSenhaRequest: ['oldPassword', 'password', 'confirmPassword'],
  toggleUsuarioModal: null,
  toggleAlterarSenhaModal: null,
});

export const UsuarioTypes = Types;
export default Creators;

export const INITIAL_STATE = Immutable({
  currentUsuario: JSON.parse(localStorage.getItem('@Nummus:usuario')) || null,
  usuarioModalOpen: false,
  alterarSenhaModalOpen: false,
});

export const submitSuccess = (state, { data }) => {
  localStorage.setItem("@Nummus:usuario", JSON.stringify(data));
  return state.merge({ currentUsuario: data });
}

export const toggleModal = state =>
  state.merge({ usuarioModalOpen: !state.usuarioModalOpen });

export const toggleAlterarSenhaModal = state =>
  state.merge({ alterarSenhaModalOpen: !state.alterarSenhaModalOpen });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SUBMIT_USUARIO_SUCCESS]: submitSuccess,
  [Types.TOGGLE_USUARIO_MODAL]: toggleModal,
  [Types.TOGGLE_ALTERAR_SENHA_MODAL]: toggleAlterarSenhaModal,
});