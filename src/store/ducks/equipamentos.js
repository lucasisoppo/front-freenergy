import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";
import { config } from "../../core/config";

const { Types, Creators } = createActions({
  getEquipamentosRequest: ["page", "perPage", "filter"],
  getEquipamentosSuccess: ["data"],
  submitEquipamentoRequest: ["id", "descricao", "potencia", "grupo_id", "setor_id"],
  submitEquipamentoSuccess: ["newEquipamento", "equipamento"],
  deleteEquipamentoRequest: ["id"],
  deleteEquipamentoSuccess: ["id"],
  setEquipamentoCurrentPage: ["page"],
  setEquipamentoFilter: ["filter"],
  toggleEquipamentoModal: null,
  toggleEquipamentoDeleteModal: null,
  toggleLoading: ["is"],
});

export const EquipamentosTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  result: {
    total: null,
    perPage: null,
    page: null,
    lastPage: null,
    data: [],
  },
  equipamentoCurrentPage: 1,
  equipamentoPerPage: config.perPage,
  equipamentoFilter: "",
  equipamentoModalOpen: false,
  equipamentoDeleteModalOpen: false,
});

export const getSuccess = (state, { data }) => state.merge({ result: data });

export const submitSuccess = (state, { newEquipamento, equipamento }) =>
  newEquipamento
    ? state.merge({
        result: {
          ...state.result,
          total: Number(state.result.total) + 1,
          data: [...state.result.data, equipamento],
        },
      })
    : state.merge({
        result: {
          ...state.result,
          data: state.result.data.map((data) => {
            return data.id === equipamento.id ? equipamento : data;
          }),
        },
      });

export const deleteSuccess = (state, { id }) =>
  state.merge({
    result: {
      ...state.result,
      total: Number(state.result.total) - 1,
      data: state.result.data.filter((item) => item.id !== id),
    },
  });

export const setCurrentPage = (state, { page }) =>
  state.merge({ equipamentoCurrentPage: page });

export const setFilter = (state, { filter }) =>
  state.merge({ equipamentoFilter: filter });

export const toggleModal = (state) =>
  state.merge({ equipamentoModalOpen: !state.equipamentoModalOpen });

export const toggleDeleteModal = (state) =>
  state.merge({
    equipamentoDeleteModalOpen: !state.equipamentoDeleteModalOpen,
  });

export const toggleLoading = (state, { is }) => state.merge({ isLoading: is });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_EQUIPAMENTOS_SUCCESS]: getSuccess,
  [Types.SUBMIT_EQUIPAMENTO_SUCCESS]: submitSuccess,
  [Types.DELETE_EQUIPAMENTO_SUCCESS]: deleteSuccess,
  [Types.SET_EQUIPAMENTO_CURRENT_PAGE]: setCurrentPage,
  [Types.SET_EQUIPAMENTO_FILTER]: setFilter,
  [Types.TOGGLE_EQUIPAMENTO_MODAL]: toggleModal,
  [Types.TOGGLE_EQUIPAMENTO_DELETE_MODAL]: toggleDeleteModal,
  [Types.TOGGLE_LOADING]: toggleLoading,
});
