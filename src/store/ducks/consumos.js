import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";
import { config } from "../../core/config";

const { Types, Creators } = createActions({
  getConsumosRequest: ["page", "perPage", "filter"],
  getConsumosSuccess: ["data"],
  submitConsumoRequest: [
    "id",
    "equipamento_id",
    "media_corrente",
    "duracao",
    "data_hora"
  
  ],
  submitConsumoSuccess: ["newConsumo", "consumo"],
  deleteConsumoRequest: ["id"],
  deleteConsumoSuccess: ["id"],
  setConsumoCurrentPage: ["page"],
  setConsumoFilter: ["filter"],
  toggleConsumoModal: null,
  toggleConsumoDeleteModal: null,
  toggleLoading: ["is"],
});

export const ConsumosTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  result: {
    total: null,
    perPage: null,
    page: null,
    lastPage: null,
    data: [],
  },
  consumoCurrentPage: 1,
  consumoPerPage: config.perPage,
  consumoFilter: "",
  consumoModalOpen: false,
  consumoDeleteModalOpen: false,
});

export const getSuccess = (state, { data }) => state.merge({ result: data });

export const submitSuccess = (state, { newConsumo, consumo }) =>
  newConsumo
    ? state.merge({
        result: {
          ...state.result,
          total: Number(state.result.total) + 1,
          data: [...state.result.data, consumo],
        },
      })
    : state.merge({
        result: {
          ...state.result,
          data: state.result.data.map((data) => {
            return data.id === consumo.id ? consumo : data;
          }),
        },
      });

export const deleteSuccess = (state, { id }) =>
  state.merge({
    result: {
      ...state.result,
      total: Number(state.result.total) - 1,
      data: state.result.data.filter((item) => item.id !== id),
    },
  });

export const setCurrentPage = (state, { page }) =>
  state.merge({ consumoCurrentPage: page });

export const setFilter = (state, { filter }) =>
  state.merge({ consumoFilter: filter });

export const toggleModal = (state) =>
  state.merge({ consumoModalOpen: !state.consumoModalOpen });

export const toggleDeleteModal = (state) =>
  state.merge({
    consumoDeleteModalOpen: !state.consumoDeleteModalOpen,
  });

export const toggleLoading = (state, { is }) => state.merge({ isLoading: is });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_CONSUMOS_SUCCESS]: getSuccess,
  [Types.SUBMIT_CONSUMO_SUCCESS]: submitSuccess,
  [Types.DELETE_CONSUMO_SUCCESS]: deleteSuccess,
  [Types.SET_CONSUMO_CURRENT_PAGE]: setCurrentPage,
  [Types.SET_CONSUMO_FILTER]: setFilter,
  [Types.TOGGLE_CONSUMO_MODAL]: toggleModal,
  [Types.TOGGLE_CONSUMO_DELETE_MODAL]: toggleDeleteModal,
  [Types.TOGGLE_LOADING]: toggleLoading,
});
