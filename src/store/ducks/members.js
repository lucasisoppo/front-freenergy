import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

const { Types, Creators } = createActions({
  toggleMembersPermissionsModal: null,
  toggleMembersInviteModal: null,
  getMembersRequest: null,
  getMembersSuccess: ['data'],
  updateMemberRequest: ['id', 'roles'],
  inviteMemberRequest: ['email'],
  toggleMemberDeleteModal: null,
});

export const MembersTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  data: [],
  membersPermissionsModalOpen: false,
  membersInviteModalOpen: false,
});

export const togglePermissionsModal = state => state.merge({ membersPermissionsModalOpen: !state.membersPermissionsModalOpen });
export const toggleInviteModal = state => state.merge({ membersInviteModalOpen: !state.membersInviteModalOpen });

export const toggleDeleteModal = state =>
  state.merge({ deleteDeleteModalOpen: !state.deleteDeleteModalOpen });

export const getSuccess = (state, { data }) => state.merge({ data });

export const updateMember = (state, { id, roles }) =>
  state.merge({
    data: state.data.map(member =>
      member.id === id ? { ...member, roles } : member
    ),
  });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.TOGGLE_MEMBERS_PERMISSIONS_MODAL]: togglePermissionsModal,
  [Types.TOGGLE_MEMBERS_INVITE_MODAL]: toggleInviteModal,
  [Types.GET_MEMBERS_SUCCESS]: getSuccess,
  [Types.UPDATE_MEMBER_REQUEST]: updateMember,
  [Types.TOGGLE_MEMBER_DELETE_MODAL]: toggleDeleteModal,
});
