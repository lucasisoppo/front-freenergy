import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import { reducer as toastr } from "react-redux-toastr";
import { reducer as auth } from "./auth";
import { reducer as usuario } from "./usuarios";
import { reducer as empresas } from "./empresas";
import { reducer as grupos } from "./grupos";
import { reducer as setors } from "./setors";
import { reducer as equipamentos } from "./equipamentos";
import { reducer as consumos } from "./consumos";
import { reducer as members } from "./members";
import { reducer as configuracoes } from "./configuracoes";

export default (history) =>
  combineReducers({
    auth,
    usuario,
    empresas,
    grupos,
    setors,
    equipamentos,
    consumos,
    configuracoes,
    members,
    toastr,
    router: connectRouter(history),
  });
