import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";
import { config } from "../../core/config";

const { Types, Creators } = createActions({
  getGruposRequest: ["page", "perPage", "filter"],
  getGruposSuccess: ["data"],
  submitGrupoRequest: ["id", "descricao", "tensao"],
  submitGrupoSuccess: ["newGrupo", "grupo"],
  deleteGrupoRequest: ["id"],
  deleteGrupoSuccess: ["id"],
  setGrupoCurrentPage: ["page"],
  setGrupoFilter: ["filter"],
  toggleGrupoModal: null,
  toggleGrupoDeleteModal: null,
  toggleLoading: ["is"]
});

export const GruposTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  result: {
    total: null,
    perPage: null,
    page: null,
    lastPage: null,
    data: []
  },
  grupoCurrentPage: 1,
  grupoPerPage: config.perPage,
  grupoFilter: "",
  grupoModalOpen: false,
  grupoDeleteModalOpen: false
});

export const getSuccess = (state, { data }) => state.merge({ result: data });

export const submitSuccess = (state, { newGrupo, grupo }) =>
  newGrupo
    ? state.merge({
        result: {
          ...state.result,
          total: Number(state.result.total) + 1,
          data: [...state.result.data, grupo]
        }
      })
    : state.merge({
        result: {
          ...state.result,
          data: state.result.data.map(data => {
            return data.id === grupo.id ? grupo : data;
          })
        }
      });

export const deleteSuccess = (state, { id }) =>
  state.merge({
    result: {
      ...state.result,
      total: Number(state.result.total) - 1,
      data: state.result.data.filter(item => item.id !== id)
    }
  });

export const setCurrentPage = (state, { page }) =>
  state.merge({ grupoCurrentPage: page });

export const setFilter = (state, { filter }) =>
  state.merge({ grupoFilter: filter });

export const toggleModal = state =>
  state.merge({ grupoModalOpen: !state.grupoModalOpen });

export const toggleDeleteModal = state =>
  state.merge({ grupoDeleteModalOpen: !state.grupoDeleteModalOpen });

export const toggleLoading = (state, { is }) => state.merge({ isLoading: is });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_GRUPOS_SUCCESS]: getSuccess,
  [Types.SUBMIT_GRUPO_SUCCESS]: submitSuccess,
  [Types.DELETE_GRUPO_SUCCESS]: deleteSuccess,
  [Types.SET_GRUPO_CURRENT_PAGE]: setCurrentPage,
  [Types.SET_GRUPO_FILTER]: setFilter,
  [Types.TOGGLE_GRUPO_MODAL]: toggleModal,
  [Types.TOGGLE_GRUPO_DELETE_MODAL]: toggleDeleteModal,
  [Types.TOGGLE_LOADING]: toggleLoading
});
