import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";
import { config } from "../../core/config";

const { Types, Creators } = createActions({
  getSetorsRequest: ["page", "perPage", "filter"],
  getSetorsSuccess: ["data"],
  submitSetorRequest: ["id", "descricao"],
  submitSetorSuccess: ["newSetor", "setor"],
  deleteSetorRequest: ["id"],
  deleteSetorSuccess: ["id"],
  setSetorCurrentPage: ["page"],
  setSetorFilter: ["filter"],
  toggleSetorModal: null,
  toggleSetorDeleteModal: null,
  toggleLoading: ["is"]
});

export const SetorsTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  result: {
    total: null,
    perPage: null,
    page: null,
    lastPage: null,
    data: []
  },
  setorCurrentPage: 1,
  setorPerPage: config.perPage,
  setorFilter: "",
  setorModalOpen: false,
  setorDeleteModalOpen: false
});

export const getSuccess = (state, { data }) => state.merge({ result: data });

export const submitSuccess = (state, { newSetor, setor }) =>
  newSetor
    ? state.merge({
        result: {
          ...state.result,
          total: Number(state.result.total) + 1,
          data: [...state.result.data, setor]
        }
      })
    : state.merge({
        result: {
          ...state.result,
          data: state.result.data.map(data => {
            return data.id === setor.id ? setor : data;
          })
        }
      });

export const deleteSuccess = (state, { id }) =>
  state.merge({
    result: {
      ...state.result,
      total: Number(state.result.total) - 1,
      data: state.result.data.filter(item => item.id !== id)
    }
  });

export const setCurrentPage = (state, { page }) =>
  state.merge({ setorCurrentPage: page });

export const setFilter = (state, { filter }) =>
  state.merge({ setorFilter: filter });

export const toggleModal = state =>
  state.merge({ setorModalOpen: !state.setorModalOpen });

export const toggleDeleteModal = state =>
  state.merge({ setorDeleteModalOpen: !state.setorDeleteModalOpen });

export const toggleLoading = (state, { is }) => state.merge({ isLoading: is });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_SETORS_SUCCESS]: getSuccess,
  [Types.SUBMIT_SETOR_SUCCESS]: submitSuccess,
  [Types.DELETE_SETOR_SUCCESS]: deleteSuccess,
  [Types.SET_SETOR_CURRENT_PAGE]: setCurrentPage,
  [Types.SET_SETOR_FILTER]: setFilter,
  [Types.TOGGLE_SETOR_MODAL]: toggleModal,
  [Types.TOGGLE_SETOR_DELETE_MODAL]: toggleDeleteModal,
  [Types.TOGGLE_LOADING]: toggleLoading
});
