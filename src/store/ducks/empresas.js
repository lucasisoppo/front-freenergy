import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";
import { config } from "../../core/config";

const { Types, Creators } = createActions({
  getEmpresasRequest: ["page", "perPage", "filter"],
  getEmpresasSuccess: ["data"],
  submitEmpresaRequest: [
    "id",
    "nome",
    "cnpj",
    "endereco",
    "cidade",
    "uf",
    "cep",
    "telefone",
    "celular",
    "email",
    "concessionaria_id",
    "tarifa_branca"
  ],
  submitEmpresaSuccess: ["newEmpresa", "empresa"],
  deleteEmpresaRequest: ["id"],
  deleteEmpresaSuccess: ["id"],
  setEmpresaCurrentPage: ["page"],
  setEmpresaFilter: ["filter"],
  toggleEmpresaModal: null,
  toggleEmpresaDeleteModal: null,
  selectEmpresa: ["empresa", "page", "perPage", "filter"],
  toggleLoading: ["is"]
});

export const EmpresasTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  result: {
    total: null,
    perPage: null,
    page: null,
    lastPage: null,
    data: []
  },
  active: localStorage.getItem("@Nummus:empresa") || null,
  empresaCurrentPage: 1,
  empresaPerPage: config.perPage,
  empresaFilter: "",
  empresaModalOpen: false,
  empresaDeleteModalOpen: false
});

export const getSuccess = (state, { data }) => state.merge({ result: data });

export const selectEmpresa = (state, { empresa }) => {
  localStorage.setItem("@Nummus:empresa", empresa);

  window.location.reload();

  return state.merge({ active: empresa });
};

export const submitSuccess = (state, { newEmpresa, empresa }) =>
  newEmpresa
    ? state.merge({
        result: {
          ...state.result,
          total: Number(state.result.total) + 1,
          data: [...state.result.data, empresa]
        }
      })
    : state.merge({
        result: {
          ...state.result,
          data: state.result.data.map(data => {
            return data.id === empresa.id ? empresa : data;
          })
        }
      });

export const deleteSuccess = (state, { id }) =>
  state.merge({
    result: {
      ...state.result,
      total: Number(state.result.total) - 1,
      data: state.result.data.filter(item => item.id !== id)
    }
  });

export const setCurrentPage = (state, { page }) =>
  state.merge({ empresaCurrentPage: page });

export const setFilter = (state, { filter }) =>
  state.merge({ empresaFilter: filter });

export const toggleModal = state =>
  state.merge({ empresaModalOpen: !state.empresaModalOpen });

export const toggleDeleteModal = state =>
  state.merge({ empresaDeleteModalOpen: !state.empresaDeleteModalOpen });

export const toggleLoading = (state, { is }) => state.merge({ isLoading: is });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_EMPRESAS_SUCCESS]: getSuccess,
  [Types.SUBMIT_EMPRESA_SUCCESS]: submitSuccess,
  [Types.DELETE_EMPRESA_SUCCESS]: deleteSuccess,
  [Types.SET_EMPRESA_CURRENT_PAGE]: setCurrentPage,
  [Types.SET_EMPRESA_FILTER]: setFilter,
  [Types.TOGGLE_EMPRESA_MODAL]: toggleModal,
  [Types.TOGGLE_EMPRESA_DELETE_MODAL]: toggleDeleteModal,
  [Types.SELECT_EMPRESA]: selectEmpresa,
  [Types.TOGGLE_LOADING]: toggleLoading
});
