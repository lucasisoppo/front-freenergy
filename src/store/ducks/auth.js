import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

const { Types, Creators } = createActions({
  signInRequest: ['email', 'password'],
  signInSuccess: ['token'],
  signOut: null,
  registroRequest: ['nome', 'cpf', 'email', 'password', 'confirmPassword'],
  resetarSenhaRequest: ['token', 'password', 'confirmPassword'],
  recuperarSenhaRequest: ['email'],
  getPermissionsSuccess: ['roles', 'permissions'],
});

export const AuthTypes = Types;
export default Creators;

export const INITIAL_STATE = Immutable({
  signedIn: !!localStorage.getItem('@Nummus:token'),
  token: localStorage.getItem('@Nummus:token') || null,
  roles: [],
  permissions: [],
});

export const success = (state, { token }) => {
  return state.merge({ signedIn: true, token });
};

export const logout = state => {
  return state.merge({ signedIn: false, token: null });
};

export const getPermissionsSuccess = (state, { roles, permissions }) =>
  state.merge({ roles, permissions });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SIGN_IN_SUCCESS]: success,
  [Types.SIGN_OUT]: logout,
  [Types.GET_PERMISSIONS_SUCCESS]: getPermissionsSuccess,
});
