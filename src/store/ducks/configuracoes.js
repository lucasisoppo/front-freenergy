import { createReducer, createActions } from "reduxsauce";
import Immutable from "seamless-immutable";
import { config } from "../../core/config";

const { Types, Creators } = createActions({
  getConfiguracoesRequest: ["page", "perPage", "filter"],
  getConfiguracoesSuccess: ["data"],
  submitConfiguracaoRequest: ["id", "tipo", "percentual", "validade_cashback"],
  submitConfiguracaoSuccess: ["newConfiguracao", "configuracao"],
  deleteConfiguracaoRequest: ["id"],
  deleteConfiguracaoSuccess: ["id"],
  setConfiguracaoCurrentPage: ["page"],
  setConfiguracaoFilter: ["filter"],
  toggleConfiguracaoModal: null,
  toggleConfiguracaoDeleteModal: null,
  toggleLoading: ["is"]
});

export const ConfiguracoesTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  result: {
    total: null,
    perPage: null,
    page: null,
    lastPage: null,
    data: []
  },
  configuracaoCurrentPage: 1,
  configuracaoPerPage: config.perPage,
  configuracaoFilter: "",
  configuracaoModalOpen: false,
  configuracaoDeleteModalOpen: false
});

export const getSuccess = (state, { data }) => state.merge({ result: data });

export const submitSuccess = (state, { newConfiguracao, configuracao }) =>
  newConfiguracao
    ? state.merge({
        result: {
          ...state.result,
          total: Number(state.result.total) + 1,
          data: [...state.result.data, configuracao]
        }
      })
    : state.merge({
        result: {
          ...state.result,
          data: state.result.data.map(data => {
            return data.id === configuracao.id ? configuracao : data;
          })
        }
      });

export const deleteSuccess = (state, { id }) =>
  state.merge({
    result: {
      ...state.result,
      total: Number(state.result.total) - 1,
      data: state.result.data.filter(item => item.id !== id)
    }
  });

export const setCurrentPage = (state, { page }) =>
  state.merge({ configuracaoCurrentPage: page });

export const setFilter = (state, { filter }) =>
  state.merge({ configuracaoFilter: filter });

export const toggleModal = state =>
  state.merge({ configuracaoModalOpen: !state.configuracaoModalOpen });

export const toggleDeleteModal = state =>
  state.merge({
    configuracaoDeleteModalOpen: !state.configuracaoDeleteModalOpen
  });

export const toggleLoading = (state, { is }) => state.merge({ isLoading: is });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_CONFIGURACOES_SUCCESS]: getSuccess,
  [Types.SUBMIT_CONFIGURACAO_SUCCESS]: submitSuccess,
  [Types.DELETE_CONFIGURACAO_SUCCESS]: deleteSuccess,
  [Types.SET_CONFIGURACAO_CURRENT_PAGE]: setCurrentPage,
  [Types.SET_CONFIGURACAO_FILTER]: setFilter,
  [Types.TOGGLE_CONFIGURACAO_MODAL]: toggleModal,
  [Types.TOGGLE_CONFIGURACAO_DELETE_MODAL]: toggleDeleteModal,
  [Types.TOGGLE_LOADING]: toggleLoading
});
