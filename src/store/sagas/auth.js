import { call, put, select } from "redux-saga/effects";
import { push } from "connected-react-router";
import { actions as toastrActions } from "react-redux-toastr";
import api from "../../services/api";

import AuthActions from "../ducks/auth";

export function* signIn({ email, password }) {
  try {
    const response = yield call(api.post, "sessions", { email, password });

    localStorage.setItem("@Nummus:token", response.data.token);

    yield put(AuthActions.signInSuccess(response.data.token));
    yield put(push("/dashboard"));
  } catch (err) {
    yield put(
      toastrActions.add({
        type: "error",
        title: "Falha na autenticação",
        message: "Seu e-mail ou senha está incorreto. "
      })
    );
  }
}

export function* registro({ nome, cpf, email, password, confirmPassword }) {
  try {
    yield call(api.post, "usuarios", {
      nome,
      cpf,
      email,
      password,
      confirmPassword
    });

    yield put(push("/login"));
    yield put(
      toastrActions.add({
        type: "success",
        title: "Registro de usuário realizado com sucesso."
      })
    );
  } catch (err) {
    const message = err.response.data.error
      ? err.response.data.error.message
      : "Esse e-mail não foi autorizado por nenhuma empresa.";
    yield put(
      toastrActions.add({
        type: "error",
        title: "Erro na operação",
        message: message
      })
    );
  }
}

export function* resetarSenha({ token, password, confirmPassword }) {
  try {
    yield call(api.put, "recuperar-senha", {
      token,
      password,
      confirmPassword
    });

    yield put(push("/dashboard"));
    yield put(
      toastrActions.add({
        type: "success",
        title: "Senha alterada com sucesso."
      })
    );
  } catch (err) {
    const message = err.response.data.error
      ? err.response.data.error.message
      : "Esse e-mail não foi autorizado por nenhuma empresa?";
    yield put(
      toastrActions.add({
        type: "error",
        title: "Erro na operação",
        message: message
      })
    );
  }
}

export function* recuperarSenha({ email }) {
  try {
    yield call(api.post, "recuperar-senha", {
      email,
      redirect_url: "http://www.nummus.com.br/reset-senha"
    });

    yield put(push("/resetar-senha"));
    yield put(
      toastrActions.add({
        type: "success",
        title: "Recuperação de senha enviado por e-mail."
      })
    );
  } catch (err) {
    yield put(
      toastrActions.add({
        type: "error",
        title: "Erro na operação",
        message: "E-mail inválido."
      })
    );
  }
}

export function* signOut() {
  localStorage.removeItem("@Nummus:token");
  localStorage.removeItem("@Nummus:empresa");
  localStorage.removeItem("@Nummus:usuario");

  yield put(push("/login"));
}

export function* getPermissions() {
  const empresa = yield select(state => state.empresas.active);
  const signedIn = yield select(state => state.auth.signedIn);

  if (!signedIn || !empresa) {
    return;
  }
  const response = yield call(api.get, "permissions");

  const { roles, permissions } = response.data;

  yield put(AuthActions.getPermissionsSuccess(roles, permissions));
}
