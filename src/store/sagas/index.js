import { all, fork, takeLatest } from "redux-saga/effects";

import {
  signIn,
  signOut,
  registro,
  getPermissions,
  recuperarSenha,
  resetarSenha,
} from "./auth";
import { AuthTypes } from "../ducks/auth";

import { getUsuario, submitUsuario, submitAlterarSenha } from "./usuarios";
import { UsuarioTypes } from "../ducks/usuarios";

import { getEmpresas, submitEmpresa, deleteEmpresa } from "./empresas";
import { EmpresasTypes } from "../ducks/empresas";

import { getGrupos, submitGrupo, deleteGrupo } from "./grupos";
import { GruposTypes } from "../ducks/grupos";

import { getSetors, submitSetor, deleteSetor } from "./setors";
import { SetorsTypes } from "../ducks/setors";

import {
  getEquipamentos,
  submitEquipamento,
  deleteEquipamento,
} from "./equipamentos";
import { EquipamentosTypes } from "../ducks/equipamentos";

import { getConsumos, submitConsumo, deleteConsumo } from "./consumos";
import { ConsumosTypes } from "../ducks/consumos";

import {
  getConfiguracoes,
  submitConfiguracao,
  deleteConfiguracao,
} from "./configuracoes";
import { ConfiguracoesTypes } from "../ducks/configuracoes";

import { getMembers, updateMember, inviteMember } from "./members";
import { MembersTypes } from "../ducks/members";

export default function* rootSaga() {
  return yield all([
    fork(getPermissions),
    takeLatest(AuthTypes.SIGN_IN_REQUEST, signIn),
    takeLatest(AuthTypes.SIGN_OUT, signOut),
    takeLatest(AuthTypes.REGISTRO_REQUEST, registro),
    takeLatest(AuthTypes.RECUPERAR_SENHA_REQUEST, recuperarSenha),
    takeLatest(AuthTypes.RESETAR_SENHA_REQUEST, resetarSenha),
    takeLatest(AuthTypes.SIGN_IN_SUCCESS, getUsuario),

    takeLatest(UsuarioTypes.SUBMIT_USUARIO_REQUEST, submitUsuario),
    takeLatest(UsuarioTypes.SUBMIT_ALTERAR_SENHA_REQUEST, submitAlterarSenha),

    takeLatest(EmpresasTypes.GET_EMPRESAS_REQUEST, getEmpresas),
    takeLatest(EmpresasTypes.SUBMIT_EMPRESA_REQUEST, submitEmpresa),
    takeLatest(EmpresasTypes.DELETE_EMPRESA_REQUEST, deleteEmpresa),

    takeLatest(EmpresasTypes.SELECT_EMPRESA, getConsumos),
    takeLatest(EmpresasTypes.SELECT_EMPRESA, getGrupos),
    takeLatest(EmpresasTypes.SELECT_EMPRESA, getSetors),
    takeLatest(EmpresasTypes.SELECT_EMPRESA, getEquipamentos),
    takeLatest(EmpresasTypes.SELECT_EMPRESA, getEquipamentos),
    takeLatest(EmpresasTypes.SELECT_EMPRESA, getPermissions),
    takeLatest(EmpresasTypes.SELECT_EMPRESA, getConfiguracoes),

    takeLatest(GruposTypes.GET_GRUPOS_REQUEST, getGrupos),
    takeLatest(GruposTypes.SUBMIT_GRUPO_REQUEST, submitGrupo),
    takeLatest(GruposTypes.DELETE_GRUPO_REQUEST, deleteGrupo),

    takeLatest(SetorsTypes.GET_SETORS_REQUEST, getSetors),
    takeLatest(SetorsTypes.SUBMIT_SETOR_REQUEST, submitSetor),
    takeLatest(SetorsTypes.DELETE_SETOR_REQUEST, deleteSetor),

    takeLatest(EquipamentosTypes.GET_EQUIPAMENTOS_REQUEST, getEquipamentos),
    takeLatest(EquipamentosTypes.SUBMIT_EQUIPAMENTO_REQUEST, submitEquipamento),
    takeLatest(EquipamentosTypes.DELETE_EQUIPAMENTO_REQUEST, deleteEquipamento),

    takeLatest(ConsumosTypes.GET_CONSUMOS_REQUEST, getConsumos),
    takeLatest(ConsumosTypes.SUBMIT_CONSUMO_REQUEST, submitConsumo),
    takeLatest(ConsumosTypes.DELETE_CONSUMO_REQUEST, deleteConsumo),

    takeLatest(ConfiguracoesTypes.GET_CONFIGURACOES_REQUEST, getConfiguracoes),
    takeLatest(
      ConfiguracoesTypes.SUBMIT_CONFIGURACAO_REQUEST,
      submitConfiguracao
    ),
    takeLatest(
      ConfiguracoesTypes.DELETE_CONFIGURACAO_REQUEST,
      deleteConfiguracao
    ),

    takeLatest(MembersTypes.GET_MEMBERS_REQUEST, getMembers),
    takeLatest(MembersTypes.UPDATE_MEMBER_REQUEST, updateMember),
    takeLatest(MembersTypes.INVITE_MEMBER_REQUEST, inviteMember),
  ]);
}
