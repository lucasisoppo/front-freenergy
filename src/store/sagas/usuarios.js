import { call, put } from 'redux-saga/effects';
import { actions as toastrActions } from 'react-redux-toastr';
import api from '../../services/api';

import UsuarioActions from '../ducks/usuarios';

export function* getUsuario() {
  const response = yield call(api.get, 'usuarios');
  const { data } = response;
  const { nome, cpf, email } = data;
  const dataFormatted = { nome, cpf, email }

  yield put(UsuarioActions.submitUsuarioSuccess(dataFormatted));
}

export function* submitUsuario({ nome, email, cpf }) {
  try {
    /* 
      Obs.: 
        Se for necessário enviar campos diferentes no post e no put, 
        altere a var dataSubmit usando dataSubmit.delete(campo) ou
        dataSubmit = { ...dataSubmit, novoCampo }, logo abaixo do 
        comando a seguir.
    */
    let dataSubmit = {
      nome,
      email,
      cpf,
    };

    yield call(api.put, 'usuarios', dataSubmit);

    yield put(UsuarioActions.submitUsuarioSuccess(dataSubmit));
    yield put(UsuarioActions.toggleUsuarioModal());
    yield put(
      toastrActions.add({
        type: 'success',
        title: 'Registro alterado com sucesso.',
      })
    );
  } catch (err) {
    let { message } = err.response.data.error;
    if (!message) {
      message = 'Erro na operação'
    }
    yield put(
      toastrActions.add({
        type: 'error',
        title: 'Erro na operação',
        message: message,
      })
    );
  }
}

export function* submitAlterarSenha({ oldPassword, password, confirmPassword }) {
  try {
    let dataSubmit = {
      oldPassword,
      password,
      confirmPassword
    };

    yield call(api.put, 'usuarios/alterar-senha', dataSubmit);
    
    yield put(UsuarioActions.toggleAlterarSenhaModal());
    yield put(
      toastrActions.add({
        type: 'success',
        title: 'Senha alterada com sucesso.',
      })
    );
  } catch (err) {
    const message = (err.response.data.error)
      ? err.response.data.error.message
      : 'Erro na operação'
    yield put(
      toastrActions.add({
        type: 'error',
        title: 'Erro na operação',
        message: message,
      })
    );
  }
}