import { call, put } from "redux-saga/effects";
import { actions as toastrActions } from "react-redux-toastr";
import api from "../../services/api";

import ConsumosActions from "../ducks/consumos";

export function* getConsumos({ page, perPage, filter }) {
  yield put(ConsumosActions.toggleLoading(true));
  let url = `consumos/${page}/${perPage}`;

  if (filter !== "") {
    url += `?filter=${filter}`;
  }

  const response = yield call(api.get, url);

  yield put(ConsumosActions.toggleLoading(false));
  yield put(ConsumosActions.getConsumosSuccess(response.data));
}

export function* submitConsumo({
  id,
  equipamento_id,
  media_corrente,
  duracao,
  data_hora
}) {
  try {
    const newConsumo = !id ? true : false;
    const callType = newConsumo ? api.post : api.put;
    const url = newConsumo ? "consumos" : `consumos/${id}`;
    /*
      Obs.:
        Se for necessário enviar campos diferentes no post e no put,
        altere a var dataSubmit usando dataSubmit.delete(campo) ou
        dataSubmit = { ...dataSubmit, novoCampo }, logo abaixo do
        comando a seguir.
    */
    let dataSubmit = {
      equipamento_id,
      media_corrente,
      duracao,
      data_hora
    };

    const response = yield call(callType, url, dataSubmit);

    yield put(ConsumosActions.submitConsumoSuccess(newConsumo, response.data));
    yield put(ConsumosActions.toggleConsumoModal());
    const toastTitle = newConsumo
      ? "Registro salvo com sucesso."
      : "Registro alterado com sucesso.";
    yield put(
      toastrActions.add({
        type: "success",
        title: toastTitle,
      })
    );
  } catch (err) {
    yield put(
      toastrActions.add({
        type: "error",
        title: "Erro na operação",
        message: "Houve um erro, tente novamente.",
      })
    );
  }
}

export function* deleteConsumo({ id }) {
  try {
    yield call(api.delete, `consumos/${id}`);
    yield put(ConsumosActions.toggleConsumoDeleteModal());
    yield put(ConsumosActions.deleteConsumoSuccess(id));
    yield put(
      toastrActions.add({
        type: "success",
        title: "Registro excluído com sucesso.",
      })
    );
  } catch (err) {
    yield put(
      toastrActions.add({
        type: "error",
        title: "Erro na operação",
        message: "Houve um erro, tente novamente.",
      })
    );
  }
}
