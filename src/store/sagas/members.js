import { call, put } from "redux-saga/effects";
import { actions as toastrActions } from "react-redux-toastr";
import api from "../../services/api";

import MembersActions from "../ducks/members";

export function* getMembers() {
  const response = yield call(api.get, "membros");

  yield put(MembersActions.getMembersSuccess(response.data));
}

export function* updateMember({ id, roles }) {
  try {
    yield call(api.put, `membros/${id}`, { roles: roles.map(role => role.id) });
    yield put(
      toastrActions.add({
        type: "success",
        title: "Colaborador atualizado.",
        message: "O colaborador foi atualizado com sucesso."
      })
    );
  } catch (err) {
    yield put(
      toastrActions.add({
        type: "error",
        title: "Erro na operação",
        message: "Houve um erro, tente novamente."
      })
    );
  }
}

export function* inviteMember({ email }) {
  try {
    yield call(api.post, "invites", { invites: [email] });
    yield put(MembersActions.toggleMembersPermissionsModal());
    yield put(MembersActions.toggleMembersInviteModal());
    yield put(
      toastrActions.add({
        type: "success",
        title: "E-mail autorizado",
        message: "O e-mail foi autorizado com sucesso."
      })
    );
  } catch (err) {
    yield put(
      toastrActions.add({
        type: "error",
        title: "Erro na operação",
        message: "Houve um erro, tente novamente."
      })
    );
  }
}
