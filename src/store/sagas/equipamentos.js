import { call, put } from "redux-saga/effects";
import { actions as toastrActions } from "react-redux-toastr";
import api from "../../services/api";

import EquipamentosActions from "../ducks/equipamentos";

export function* getEquipamentos({ page, perPage, filter }) {
  yield put(EquipamentosActions.toggleLoading(true));
  let url = `equipamentos/${page}/${perPage}`;

  if (filter !== "") {
    url += `?filter=${filter}`;
  }

  const response = yield call(api.get, url);

  yield put(EquipamentosActions.toggleLoading(false));
  yield put(EquipamentosActions.getEquipamentosSuccess(response.data));
}

export function* submitEquipamento({ id, descricao, potencia, grupo_id , setor_id }) {
  try {
    const newEquipamento = !id ? true : false;
    const callType = newEquipamento ? api.post : api.put;
    const url = newEquipamento ? "equipamentos" : `equipamentos/${id}`;
    /*
      Obs.:
        Se for necessário enviar campos diferentes no post e no put,
        altere a var dataSubmit usando dataSubmit.delete(campo) ou
        dataSubmit = { ...dataSubmit, novoCampo }, logo abaixo do
        comando a seguir.
    */
    let dataSubmit = {
      descricao,
      potencia,
      grupo_id,
      setor_id
    };

    const response = yield call(callType, url, dataSubmit);

    yield put(
      EquipamentosActions.submitEquipamentoSuccess(
        newEquipamento,
        response.data
      )
    );
    yield put(EquipamentosActions.toggleEquipamentoModal());
    const toastTitle = newEquipamento
      ? "Registro salvo com sucesso."
      : "Registro alterado com sucesso.";
    yield put(
      toastrActions.add({
        type: "success",
        title: toastTitle,
      })
    );
  } catch (err) {
    yield put(
      toastrActions.add({
        type: "error",
        title: "Erro na operação",
        message: "Houve um erro, tente novamente.",
      })
    );
  }
}

export function* deleteEquipamento({ id }) {
  try {
    yield call(api.delete, `equipamentos/${id}`);
    yield put(EquipamentosActions.toggleEquipamentoDeleteModal());
    yield put(EquipamentosActions.deleteEquipamentoSuccess(id));
    yield put(
      toastrActions.add({
        type: "success",
        title: "Registro excluído com sucesso.",
      })
    );
  } catch (err) {
    yield put(
      toastrActions.add({
        type: "error",
        title: "Erro na operação",
        message: "Houve um erro, tente novamente.",
      })
    );
  }
}
