import { call, put } from "redux-saga/effects";
import { actions as toastrActions } from "react-redux-toastr";
import api from "../../services/api";

import SetorsActions from "../ducks/setors";

export function* getSetors({ page, perPage, filter }) {
  yield put(SetorsActions.toggleLoading(true));
  let url = `setors/${page}/${perPage}`;

  if (filter !== "") {
    url += `?filter=${filter}`;
  }

  const response = yield call(api.get, url);

  yield put(SetorsActions.toggleLoading(false));
  yield put(SetorsActions.getSetorsSuccess(response.data));
}

export function* submitSetor({ id, descricao }) {
  try {
    const newSetor = !id ? true : false;
    const callType = newSetor ? api.post : api.put;
    const url = newSetor ? "setors" : `setors/${id}`;
    /*
      Obs.:
        Se for necessário enviar campos diferentes no post e no put,
        altere a var dataSubmit usando dataSubmit.delete(campo) ou
        dataSubmit = { ...dataSubmit, novoCampo }, logo abaixo do
        comando a seguir.
    */
    let dataSubmit = {
      descricao,
    };

    const response = yield call(callType, url, dataSubmit);

    yield put(SetorsActions.submitSetorSuccess(newSetor, response.data));
    yield put(SetorsActions.toggleSetorModal());
    const toastTitle = newSetor
      ? "Registro salvo com sucesso."
      : "Registro alterado com sucesso.";
    yield put(
      toastrActions.add({
        type: "success",
        title: toastTitle
      })
    );
  } catch (err) {
    yield put(
      toastrActions.add({
        type: "error",
        title: "Erro na operação",
        message: "Houve um erro, tente novamente."
      })
    );
  }
}

export function* deleteSetor({ id }) {
  try {
    yield call(api.delete, `setors/${id}`);
    yield put(SetorsActions.toggleSetorDeleteModal());
    yield put(SetorsActions.deleteSetorSuccess(id));
    yield put(
      toastrActions.add({
        type: "success",
        title: "Registro excluído com sucesso."
      })
    );
  } catch (err) {
    yield put(
      toastrActions.add({
        type: "error",
        title: "Erro na operação",
        message: "Houve um erro, tente novamente."
      })
    );
  }
}
