import { call, put } from 'redux-saga/effects';
import { actions as toastrActions } from 'react-redux-toastr';
import api from '../../services/api';

import ConfiguracoesActions from '../ducks/configuracoes';

export function* getConfiguracoes({ page, perPage, filter }) {
  yield put(ConfiguracoesActions.toggleLoading(true));
  let url = `configuracoes/${page}/${perPage}`;

  if (filter !== '') {
    url += `?filter=${filter}`;
  }

  const response = yield call(api.get, url);

  yield put(ConfiguracoesActions.toggleLoading(false));
  yield put(ConfiguracoesActions.getConfiguracoesSuccess(response.data));
}

export function* submitConfiguracao({
  id,
  tipo,
  percentual,
  validade_cashback,
}) {
  try {
    const newConfiguracao = !id ? true : false;
    const callType = newConfiguracao ? api.post : api.put;
    const url = newConfiguracao ? 'configuracoes' : `configuracoes/${id}`;

    /* 
      Obs.: 
        Se for necessário enviar campos diferentes no post e no put, 
        altere a var dataSubmit usando dataSubmit.delete(campo) ou
        dataSubmit = { ...dataSubmit, novoCampo }, logo abaixo do 
        comando a seguir.
    */
    let dataSubmit = {
      tipo,
      percentual,
      validade_cashback,
    };

    const response = yield call(callType, url, dataSubmit);

    yield put(
      ConfiguracoesActions.submitConfiguracaoSuccess(
        newConfiguracao,
        response.data
      )
    );
    yield put(ConfiguracoesActions.toggleConfiguracaoModal());
    const toastTitle = newConfiguracao
      ? 'Registro salvo com sucesso.'
      : 'Registro alterado com sucesso.';
    yield put(
      toastrActions.add({
        type: 'success',
        title: toastTitle,
      })
    );
  } catch (err) {
    yield put(
      toastrActions.add({
        type: 'error',
        title: 'Erro na operação',
        message: 'Houve um erro, tente novamente.',
      })
    );
  }
}

export function* deleteConfiguracao({ id }) {
  try {
    yield call(api.delete, `configuracoes/${id}`);
    yield put(ConfiguracoesActions.toggleConfiguracaoDeleteModal());
    yield put(ConfiguracoesActions.deleteConfiguracaoSuccess(id));
    yield put(
      toastrActions.add({
        type: 'success',
        title: 'Registro excluído com sucesso.',
      })
    );
  } catch (err) {
    yield put(
      toastrActions.add({
        type: 'error',
        title: 'Erro na operação',
        message: 'Houve um erro, tente novamente.',
      })
    );
  }
}
