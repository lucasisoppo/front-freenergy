import { call, put } from "redux-saga/effects";
import { actions as toastrActions } from "react-redux-toastr";
import api from "../../services/api";

import EmpresasActions from "../ducks/empresas";

export function* getEmpresas({ page, perPage, filter }) {
  yield put(EmpresasActions.toggleLoading(true));
  let url = `empresas/${page}/${perPage}`;

  if (filter !== "") {
    url += `?filter=${filter}`;
  }

  const response = yield call(api.get, url);

  yield put(EmpresasActions.toggleLoading(false));
  yield put(EmpresasActions.getEmpresasSuccess(response.data));
}

export function* submitEmpresa({
  id,
  nome,
  cnpj,
  endereco,
  cidade,
  uf,
  cep,
  telefone,
  celular,
  email,
  concessionaria_id,
  tarifa_branca
}) {
  try {
    const newEmpresa = !id ? true : false;
    const callType = newEmpresa ? api.post : api.put;
    const url = newEmpresa ? "empresas" : `empresas/${id}`;

    /*
      Obs.:
        Se for necessário enviar campos diferentes no post e no put,
        altere a var dataSubmit usando dataSubmit.delete(campo) ou
        dataSubmit = { ...dataSubmit, novoCampo }, logo abaixo do
        comando a seguir.
    */
    let dataSubmit = {
      nome,
      cnpj,
      endereco,
      cidade,
      uf,
      cep,
      telefone,
      celular,
      email,
      concessionaria_id,
      tarifa_branca
    };

    const response = yield call(callType, url, dataSubmit);

    yield put(EmpresasActions.submitEmpresaSuccess(newEmpresa, response.data));
    yield put(EmpresasActions.toggleEmpresaModal());
    const toastTitle = newEmpresa
      ? "Registro salvo com sucesso."
      : "Registro alterado com sucesso.";
    yield put(
      toastrActions.add({
        type: "success",
        title: toastTitle
      })
    );
  } catch (err) {
    yield put(
      toastrActions.add({
        type: "error",
        title: "Erro na operação",
        message: "Houve um erro, tente novamente."
      })
    );
  }
}

export function* deleteEmpresa({ id }) {
  try {
    yield call(api.delete, `empresas/${id}`);
    yield put(EmpresasActions.toggleEmpresaDeleteModal());
    yield put(EmpresasActions.deleteEmpresaSuccess(id));
    yield put(
      toastrActions.add({
        type: "success",
        title: "Registro excluído com sucesso."
      })
    );
  } catch (err) {
    yield put(
      toastrActions.add({
        type: "error",
        title: "Erro na operação",
        message: "Houve um erro, tente novamente."
      })
    );
  }
}
