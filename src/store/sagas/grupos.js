import { call, put } from "redux-saga/effects";
import { actions as toastrActions } from "react-redux-toastr";
import api from "../../services/api";

import GruposActions from "../ducks/grupos";

export function* getGrupos({ page, perPage, filter }) {
  yield put(GruposActions.toggleLoading(true));
  let url = `grupos/${page}/${perPage}`;

  if (filter !== "") {
    url += `?filter=${filter}`;
  }

  const response = yield call(api.get, url);

  yield put(GruposActions.toggleLoading(false));
  yield put(GruposActions.getGruposSuccess(response.data));
}

export function* submitGrupo({ id, descricao, setor, tensao }) {
  try {
    const newGrupo = !id ? true : false;
    const callType = newGrupo ? api.post : api.put;
    const url = newGrupo ? "grupos" : `grupos/${id}`;
    /*
      Obs.:
        Se for necessário enviar campos diferentes no post e no put,
        altere a var dataSubmit usando dataSubmit.delete(campo) ou
        dataSubmit = { ...dataSubmit, novoCampo }, logo abaixo do
        comando a seguir.
    */
    let dataSubmit = {
      descricao,
      setor,
      tensao
    };

    const response = yield call(callType, url, dataSubmit);

    yield put(GruposActions.submitGrupoSuccess(newGrupo, response.data));
    yield put(GruposActions.toggleGrupoModal());
    const toastTitle = newGrupo
      ? "Registro salvo com sucesso."
      : "Registro alterado com sucesso.";
    yield put(
      toastrActions.add({
        type: "success",
        title: toastTitle
      })
    );
  } catch (err) {
    yield put(
      toastrActions.add({
        type: "error",
        title: "Erro na operação",
        message: "Houve um erro, tente novamente."
      })
    );
  }
}

export function* deleteGrupo({ id }) {
  try {
    yield call(api.delete, `grupos/${id}`);
    yield put(GruposActions.toggleGrupoDeleteModal());
    yield put(GruposActions.deleteGrupoSuccess(id));
    yield put(
      toastrActions.add({
        type: "success",
        title: "Registro excluído com sucesso."
      })
    );
  } catch (err) {
    yield put(
      toastrActions.add({
        type: "error",
        title: "Erro na operação",
        message: "Houve um erro, tente novamente."
      })
    );
  }
}
