import styled from 'styled-components';

const FabButton = styled.button`
  cursor: pointer;
  position: fixed;
  bottom: 10px;
  right: 10px;
  width: 50px;
  height: 50px;
  border-radius: 50%;
  border: none;
  box-shadow: 0 1px 5px rgba(0, 0, 0, 0.6);
  font-size: 24px;
  color: #fff;
  background: #0c9e3f;
  webkit-transition: 0.25s ease-out;
  -moz-transition: 0.25s ease-out;
  transition: 0.25s ease-out;

  &:hover {
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.7);
    /*transform: rotate(90deg);*/
  }
`;

export default FabButton;
