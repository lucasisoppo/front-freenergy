import styled from 'styled-components';

const ButtonIconOnly = styled.button`
  background: transparent;
  border: 0;
  padding: 5px;
  border-radius: 50%;
  transition: background-color 0.15s ease;

  &:hover {
    background: rgba(255, 255, 255, 0.1);
  }
`;

export default ButtonIconOnly;
