const locale = "pt-BR";

export const { format: formatPrice } = new Intl.NumberFormat(locale, {
  style: "currency",
  currency: "BRL",
});

export const { format: formatDouble } = new Intl.NumberFormat(locale, {
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});

export const { format: formatInt } = new Intl.NumberFormat(locale, {
  maximumFractionDigits: 0,
});

export function formatDayMonth(data) {
  const dia = data.substring(8, 10);
  const mes = data.substring(5, 7);
  return dia + "/" + mes;
}

export function formatDate(data) {
  const dia = data.substring(8, 10);
  const mes = data.substring(5, 7);
  const ano = data.substring(0, 4);
  return dia + "/" + mes + "/" + ano;
}

export function formatDateHour(data) {
  const dia = data.substring(8, 10);
  const mes = data.substring(5, 7);
  const ano = data.substring(0, 4);
  const hora = data.substring(11, 13);
  return dia + "/" + mes + "/" + ano + ", " + hora + "h";
}

export function formatHourMinutes(data) {
  const hora = data.substring(11, 13);
  const minuto = data.substring(14, 16);
  return hora + ":" + minuto + "h";
}

export function formatDateHourMinutes(data) {
  const dia = data.substring(8, 10);
  const mes = data.substring(5, 7);
  const ano = data.substring(0, 4);
  const hora = data.substring(11, 13);
  const minuto = data.substring(14, 16);
  return dia + "/" + mes + "/" + ano + ", " + hora + ":" + minuto + "h";
}

export function formatWeek(data) {
  const semana = [
    "Domingo",
    "Segunda-Feira",
    "Terça-Feira",
    "Quarta-Feira",
    "Quinta-Feira",
    "Sexta-Feira",
    "Sábado",
  ];
  data = new Date(data);
  return semana[data.getDay()];
}
