import React from "react";

const Dashboard = React.lazy(() => import("../views/Dashboard"));
const Consumos = React.lazy(() => import("../views/Consumos"));
const Equipamentos = React.lazy(() => import("../views/Equipamentos"));
const Grupos = React.lazy(() => import("../views/Grupos"));
const Setors = React.lazy(() => import("../views/Setors"));
const Alertas = React.lazy(() => import("../views/Alertas"));
const Empresas = React.lazy(() => import("../views/Empresas"));

const routes = [
  { path: "/", exact: true, name: "Início" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/consumos", name: "Consumos", component: Consumos },
  { path: "/equipamentos", name: "Equipamentos", component: Equipamentos },
  { path: "/grupos", name: "Grupos", component: Grupos },
  { path: "/setors", name: "Setors", component: Setors },
  { path: "/alertas", name: "Alertas", component: Alertas },
  { path: "/empresas", name: "Empresas", component: Empresas }
];

export default routes;
