import React from 'react';
import { ConnectedRouter } from 'connected-react-router';
import { Switch } from 'react-router-dom';

import history from './history';
import Private from './private';
import Guest from './guest';

const SignIn = React.lazy(() => import('../views/Auth/SignIn'));
const Registros = React.lazy(() => import('../views/Auth/Registro'));
const RecuperarSenha = React.lazy(() => import('../views/Auth/RecuperarSenha'));
const ResetarSenha = React.lazy(() => import('../views/Auth/ResetarSenha'));
const DefaultLayout = React.lazy(() => import('../containers/DefaultLayout'));

const loading = () => <div className="animated fadeIn pt-3 text-center">Carregando...</div>;

const Routes = () => (
  <ConnectedRouter history={history}>
    <React.Suspense fallback={loading()}>
      <Switch>
        <Guest exact path="/login" component={SignIn} />
        <Guest exact path="/registro" component={Registros} />
        <Guest exact path="/recuperar-senha" component={RecuperarSenha} />
        <Guest exact path="/resetar-senha" component={ResetarSenha} />
        <Private path="/" name="Home" component={DefaultLayout} />
      </Switch>
    </React.Suspense>
  </ConnectedRouter>
);

export default Routes;
