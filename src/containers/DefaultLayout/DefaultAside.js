import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import MembersActions from "../../store/ducks/members";
import api from "../../services/api";

import {
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  ListGroup,
  ListGroupItem,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  Label,
  Input,
  FormGroup,
  FormFeedback,
  CustomInput,
  Alert,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from "reactstrap";
import PropTypes from "prop-types";
import classNames from "classnames";

const propTypes = {
  children: PropTypes.node
};

const defaultProps = {};

class DefaultAside extends Component {
  initial_state = {
    invite: "",
    triedToSubmit: false,
    memberEdit: ""
    /*{
      usuario: [],
      roles: [],
    },*/
  };

  state = {
    ...this.initial_state,
    roles: [],
    activeTab: "1"
  };

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  async componentDidMount() {
    const { activeEmpresa } = this.props;
    if (!activeEmpresa) return;

    const { getMembersRequest } = this.props;
    getMembersRequest();

    const response = await api.get("roles");
    this.setState({ roles: response.data });
  }

  handleInvite() {
    this.clearFields();
    this.toggleInviteModal();
  }

  handleSubmitInvite = e => {
    e.preventDefault();

    this.setState({ triedToSubmit: true });
    const { inviteMemberRequest } = this.props;
    const { invite } = this.state;

    inviteMemberRequest(invite);
  };

  handleRolesChange = (e, role) => {
    const checked = e.target.checked;
    console.log(checked);

    const roles = checked
      ? [...this.state.memberEdit.roles, role]
      : this.state.memberEdit.roles.filter(
          memberRole => memberRole.id !== role.id
        );

    this.setState({
      memberEdit: { ...this.state.memberEdit, roles }
    });

    const { updateMemberRequest } = this.props;
    const { memberEdit } = this.state;

    updateMemberRequest(memberEdit.id, roles);
  };

  togglePermissionsModal(member) {
    this.setState({ memberEdit: member });
    const { toggleMembersPermissionsModal } = this.props;
    toggleMembersPermissionsModal();
  }

  toggleInviteModal() {
    const { toggleMembersInviteModal } = this.props;
    toggleMembersInviteModal();
  }

  clearFields() {
    this.setState(this.initial_state);
  }

  cancelSubmitInvite() {
    this.clearFields();
    this.toggleInviteModal();
  }

  cancelSubmitPermissions() {
    this.clearFields();
    this.togglePermissionsModal();
  }

  render() {
    const { members, activeEmpresa } = this.props;
    const { roles, invite, triedToSubmit, memberEdit } = this.state;

    return (
      <React.Fragment>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classNames({ active: this.state.activeTab === "1" })}
              onClick={() => {
                this.toggle("1");
              }}
            >
              <i className="icon-user"></i>
            </NavLink>
          </NavItem>
          {/* <NavItem>
            <NavLink
              className={classNames({ active: this.state.activeTab === '2' })}
              onClick={() => { this.toggle('2'); }}
            >
              <i className="icon-bell">
                <span className="badge badge-danger badge-pill">2</span>
              </i>
            </NavLink>
          </NavItem> */}
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          {/* LISTA DE MEMBROS */}
          <TabPane tabId="1">
            <ListGroup className="list-group-accent" tag={"div"}>
              <ListGroupItem className="list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">
                Membros
              </ListGroupItem>
              {!activeEmpresa ? (
                <div className="text-muted text-center my-5">
                  Selecione uma empresa para ver os membros
                </div>
              ) : (
                members.data.map(member => (
                  <ListGroupItem
                    key={member.id}
                    action
                    className="list-group-item-divider cursor-pointer"
                    onClick={() => this.togglePermissionsModal(member)}
                  >
                    <span>{member.usuario.nome}</span>
                    <i className="fa fa-cog float-right"></i>
                  </ListGroupItem>
                ))
              )}
            </ListGroup>
            <div className="p-3">
              <Button
                color="success"
                block
                size="lg"
                onClick={() => this.handleInvite()}
              >
                Autorizar
              </Button>
            </div>
          </TabPane>

          {/* <TabPane tabId="2"> */}
          {/* <ListGroup className="list-group-accent" tag={"div"}>
              <ListGroupItem className="list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">
                Today
              </ListGroupItem>
              <ListGroupItem
                action
                tag="a"
                href="#"
                className="list-group-item-accent-warning list-group-item-divider"
              >
                <div>
                  Resgate realizado de <strong>R$154.33</strong>
                </div>
              </ListGroupItem>
              <ListGroupItem
                action
                tag="a"
                href="#"
                className="list-group-item-accent-primary list-group-item-divider"
              >
                <div>
                  Gerou cashback de <strong> R$12,00 </strong>
                </div>
              </ListGroupItem>
            </ListGroup> */}
          {/* </TabPane> */}
        </TabContent>

        {/* MODAL PARA CONVITES */}
        <Modal
          isOpen={members.membersInviteModalOpen}
          toggle={() => this.toggleInviteModal()}
        >
          <Form onSubmit={e => this.handleSubmitInvite(e)}>
            <ModalHeader toggle={() => this.toggleInviteModal()}>
              Autorização e-mail
            </ModalHeader>
            <ModalBody>
              <Alert color="success">
                Após autorização de e-mail, o colaborador pode fazer o registro
                do usuário até 24 horas.
              </Alert>
              <Label for="invite">E-mail</Label>
              <InputGroup className="mb-3">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className="fa fa-envelope-o"></i>
                  </InputGroupText>
                </InputGroupAddon>
                <Input
                  type="email"
                  id="invite"
                  name="invite"
                  value={invite}
                  onChange={e => this.setState({ invite: e.target.value })}
                  placeholder="Digite o e-mail do colaborador."
                  invalid={triedToSubmit && invite === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </InputGroup>
            </ModalBody>
            <ModalFooter>
              <Button color="success" type="submit">
                Autorizar
              </Button>{" "}
              <Button
                color="secondary"
                onClick={() => this.cancelSubmitInvite()}
              >
                Cancelar
              </Button>
            </ModalFooter>
          </Form>
        </Modal>

        {/* MODAL PARA PERMISSIONS */}
        {memberEdit && (
          <Modal
            isOpen={members.membersPermissionsModalOpen}
            toggle={() => this.togglePermissionsModal(null)}
          >
            <ModalHeader toggle={() => this.togglePermissionsModal(null)}>
              Permissões para {memberEdit.usuario.nome}
            </ModalHeader>
            <ModalBody>
              <FormGroup>
                <div>
                  {roles.map(role => (
                    <CustomInput
                      key={role.id}
                      type="switch"
                      id={role.id}
                      name={role.id}
                      label={role.nome}
                      checked={
                        !!memberEdit.roles.find(
                          memberRole => memberRole.id === role.id
                        )
                      }
                      onChange={e => this.handleRolesChange(e, role)}
                    />
                  ))}
                </div>
              </FormGroup>
            </ModalBody>
            <ModalFooter>
              <Button
                color="secondary"
                onClick={() => this.cancelSubmitPermissions()}
              >
                Fechar
              </Button>
            </ModalFooter>
          </Modal>
        )}
      </React.Fragment>
    );
  }
}

DefaultAside.propTypes = propTypes;
DefaultAside.defaultProps = defaultProps;

const mapStateToProps = state => ({
  members: state.members,
  activeEmpresa: state.empresas.active
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(MembersActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(DefaultAside);
