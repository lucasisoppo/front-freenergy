import React, { Component } from "react";

import Can from "../../components/Can";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import EmpresasActions from "../../store/ducks/empresas";
import AuthActions from "../../store/ducks/auth";
import UsuarioActions from "../../store/ducks/usuarios";
// import { NavLink } from "react-router-dom";
import {
  UncontrolledDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  // NavItem,
  Form,
  FormGroup,
  FormFeedback,
  Label,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import PropTypes from "prop-types";
import {
  AppAsideToggler,
  AppNavbarBrand,
  AppSidebarToggler,
} from "@coreui/react";
import favicon from "../../assets/favicon.svg";
import logo from "../../assets/logo.svg";
import { cpfMask } from "../../util/mask";

const propTypes = {
  children: PropTypes.node,
  getEmpresasRequest: PropTypes.func.isRequired,
  selectEmpresa: PropTypes.func.isRequired,
  submitUsuarioRequest: PropTypes.func.isRequired,
};

const defaultProps = {};

class DefaultHeader extends Component {
  initial_state = {
    nome: "",
    email: "",
    cpf: "",
    oldPassword: "",
    password: "",
    confirmPassword: "",
    triedToSubmit: false,
  };

  state = {
    ...this.initial_state,
    selectEmpresa: {},
  };

  async componentDidMount() {
    // const { empresas, selectEmpresa } = this.props;
    const { empresas } = this.props;
    this.getData();
    this.setState({ empresa_database: empresas.active });
  }

  getData() {
    const { empresas, getEmpresasRequest } = this.props;
    getEmpresasRequest(
      empresas.empresaCurrentPage,
      empresas.empresaPerPage,
      empresas.empresaFilter
    );
  }

  handleEmpresaSelect(empresa) {
    if (empresa === null || empresa === "Selecione uma empresa") {
      return;
    }
    this.setState({ empresa_database: empresa });

    const { selectEmpresa } = this.props;

    selectEmpresa(empresa, 1, 20, "");
  }

  /* Alterar Perfil */
  handleUsuarioSubmit(e) {
    e.preventDefault();

    this.setState({ triedToSubmit: true });
    const { submitUsuarioRequest } = this.props;
    const { nome, email, cpf } = this.state;

    submitUsuarioRequest(nome, email, cpf);
  }

  handleUsuarioEdit() {
    const { usuario } = this.props;

    this.setState({
      ...usuario.currentUsuario,
    });

    this.toggleUsuarioModal();
  }

  toggleUsuarioModal() {
    const { toggleUsuarioModal } = this.props;
    toggleUsuarioModal();
  }

  clearUsuarioFields() {
    this.setState(this.initial_state);
  }

  cancelUsuarioSubmit = () => {
    this.clearUsuarioFields();
    this.toggleUsuarioModal();
  };

  /* Alteração de senha */

  handleAlterarSenhaSubmit(e) {
    e.preventDefault();

    this.setState({ triedToSubmit: true });
    const { submitAlterarSenhaRequest } = this.props;
    const { oldPassword, password, confirmPassword } = this.state;

    submitAlterarSenhaRequest(oldPassword, password, confirmPassword);
  }

  toggleAlterarSenhaModal() {
    this.clearAlterarSenhaFields();
    const { toggleAlterarSenhaModal } = this.props;
    toggleAlterarSenhaModal();
  }

  clearAlterarSenhaFields() {
    this.setState(this.initial_state);
  }

  cancelAlterarSenhaSubmit = () => {
    this.clearAlterarSenhaFields();
    this.toggleAlterarSenhaModal();
  };

  render() {
    // eslint-disable-next-line
    const {
      // children,
      // ...attributes,
      empresas,
      usuario,
    } = this.props;

    const {
      empresa_database,
      nome,
      email,
      cpf,
      oldPassword,
      password,
      confirmPassword,
      triedToSubmit,
    } = this.state;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          className="d-none d-lg-flex"
          full={{ src: logo, width: 170, height: 30, alt: "Free energy" }}
          minimized={{
            src: favicon,
            width: 45,
            height: 45,
            alt: "Free energy",
          }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <FormGroup className="my-2" style={{ flex: 1, maxWidth: "300px" }}>
          <Input
            type="select"
            name="empresas"
            id="empresas"
            value={empresa_database}
            onChange={(e) => this.handleEmpresaSelect(e.target.value)}
          >
            <option value={null}>Selecione uma empresa</option>
            {empresas.result.data.map((empresa) => (
              <option key={empresa.id} value={empresa.database}>
                {empresa.nome}
              </option>
            ))}
          </Input>
        </FormGroup>

        {/* <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink to="/consumos" className="nav-link">
              <i className="fa fa-align-left"></i> Consumo
            </NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink to="/equipamentos" className="nav-link">
              <i className="fa fa-cog"></i> Equipamentos
            </NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink to="/grupos" className="nav-link">
              <i className="fa fa-cogs"></i> Grupos
            </NavLink>
          </NavItem>
        </Nav> */}

        <Nav className="ml-auto" navbar>
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <img
                src={"../../assets/img/avatars/1.png"}
                className="img-avatar"
                alt="admin@bootstrapmaster.com"
              />
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem header tag="div" className="text-center">
                <strong>Conta</strong>
              </DropdownItem>
              <DropdownItem onClick={() => this.handleUsuarioEdit()}>
                <i className="fa fa-user"></i> Alterar Perfil
              </DropdownItem>
              <DropdownItem onClick={() => this.toggleAlterarSenhaModal()}>
                <i className="fa fa-lock"></i> Alterar Senha
              </DropdownItem>
              <DropdownItem onClick={(e) => this.props.signOut(e)}>
                <i className="fa fa-sign-out"></i> Sair
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
        <Can checkRole="admin">
          <AppAsideToggler className="d-md-down-none" />
        </Can>
        <AppAsideToggler className="d-lg-none" mobile />

        {/* Modal Alterar Pefil */}
        <Modal
          isOpen={usuario.usuarioModalOpen}
          toggle={() => this.toggleUsuarioModal()}
        >
          <Form onSubmit={(e) => this.handleUsuarioSubmit(e)}>
            <ModalHeader toggle={() => this.toggleUsuarioModal()}>
              Alterando perfil
            </ModalHeader>
            <ModalBody>
              <FormGroup>
                <Label for="nome">Nome</Label>
                <Input
                  type="text"
                  id="nome"
                  name="nome"
                  value={nome}
                  onChange={(e) => this.setState({ nome: e.target.value })}
                  placeholder="Nome completo"
                  invalid={triedToSubmit && nome === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label for="email">E-mail</Label>
                <Input
                  type="email"
                  id="email"
                  name="email"
                  value={email}
                  onChange={(e) => this.setState({ email: e.target.value })}
                  placeholder="exemplo@exemplo.com"
                  invalid={triedToSubmit && email === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label for="cpf">CPF</Label>
                <Input
                  type="text"
                  id="cpf"
                  name="cpf"
                  value={cpf}
                  onChange={(e) =>
                    this.setState({ cpf: cpfMask(e.target.value) })
                  }
                  placeholder="000.000.000-00"
                  maxLength="14"
                  invalid={triedToSubmit && cpf === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
            </ModalBody>
            <ModalFooter>
              <Button color="success" type="submit">
                Salvar
              </Button>{" "}
              <Button
                color="secondary"
                onClick={() => this.cancelUsuarioSubmit()}
              >
                Cancelar
              </Button>
            </ModalFooter>
          </Form>
        </Modal>

        {/* Modal Alterar Senha */}
        <Modal
          isOpen={usuario.alterarSenhaModalOpen}
          toggle={() => this.toggleAlterarSenhaModal()}
        >
          <Form onSubmit={(e) => this.handleAlterarSenhaSubmit(e)}>
            <ModalHeader toggle={() => this.toggleAlterarSenhaModal()}>
              Alterando senha
            </ModalHeader>
            <ModalBody>
              <FormGroup>
                <Label for="oldPassword">Senha antiga</Label>
                <Input
                  type="password"
                  id="oldPassword"
                  name="oldPassword"
                  value={oldPassword}
                  onChange={(e) =>
                    this.setState({ oldPassword: e.target.value })
                  }
                  placeholder="********"
                  invalid={triedToSubmit && oldPassword === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label for="password">Nova senha</Label>
                <Input
                  type="password"
                  id="password"
                  name="password"
                  value={password}
                  onChange={(e) => this.setState({ password: e.target.value })}
                  placeholder="********"
                  invalid={triedToSubmit && password === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label for="confirmPassword">Confirmar senha</Label>
                <Input
                  type="password"
                  id="confirmPassword"
                  name="confirmPassword"
                  value={confirmPassword}
                  onChange={(e) =>
                    this.setState({ confirmPassword: e.target.value })
                  }
                  placeholder="********"
                  invalid={triedToSubmit && confirmPassword === ""}
                />
                <FormFeedback>Campo obrigatório</FormFeedback>
              </FormGroup>
            </ModalBody>
            <ModalFooter>
              <Button color="success" type="submit">
                Salvar
              </Button>{" "}
              <Button
                color="secondary"
                onClick={() => this.cancelAlterarSenhaSubmit()}
              >
                Cancelar
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

const mapStateToProps = (state) => ({
  empresas: state.empresas,
  usuario: state.usuario,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    { ...EmpresasActions, ...AuthActions, ...UsuarioActions },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(DefaultHeader);
