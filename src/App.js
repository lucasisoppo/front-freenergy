import React, { Fragment, Component } from 'react';
import { Provider } from 'react-redux';
import ReduxToastr from 'react-redux-toastr';

import store from './store';
import Routes from './routes';

import './App.scss';

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <Fragment>
          <Routes />
          <ReduxToastr
            timeOut={3000}
            transitionIn="bounceIn"
            transitionOut="bounceOut"
            progressBar
            closeOnToastrClick
          />
        </Fragment>
      </Provider>
    );
  }
}

export default App;
